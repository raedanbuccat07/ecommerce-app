import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import 'animate.css';

//import the bootstrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-multi-carousel/lib/styles.css';
import 'react-bootstrap-table2-filter/dist/react-bootstrap-table2-filter.min.css';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import 'react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css';
import "react-responsive-carousel/lib/styles/carousel.min.css";
import './App.css';


{/* <React.StrictMode>
<App />
</React.StrictMode> */}

ReactDOM.render(
  <App />,
  document.getElementById('root')
);