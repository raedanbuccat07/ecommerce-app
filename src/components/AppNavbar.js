import { Navbar, Nav, NavDropdown, Container } from 'react-bootstrap'
import { Image } from 'cloudinary-react';
import React, { useContext, Fragment, useState, useEffect } from 'react'
import { NavLink, useHistory } from 'react-router-dom'
import UserContext from '../UserContext'
import Avatar from 'react-avatar';
import Swal from 'sweetalert2'

export default function AppNavbar() {

    const { user, setUser } = useContext(UserContext)
    const [avatarPicture, setAvatarPicture] = useState("")

    const history = useHistory()

    function importAll(r) {
        return r.keys().map(r)
    }

    const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/))

    useEffect(() => {
        if (localStorage.getItem('profilePictureLocal') != null) {
            setAvatarPicture(localStorage.getItem('profilePictureLocal'))
        } else if (user.profilePicture != null) {
            setAvatarPicture(user.profilePicture)
        }

    }, [])

    function logoutClick(e) {
        e.preventDefault()
        Swal.fire({
            title: "Logout",
            icon: "question",
            text: "Are you sure you want to logout?.",
            color: 'white',
            background: 'rgb(125, 125, 125)',
            backdrop: `url("${images[2]}") right bottom no-repeat`,
            showClass: {
                popup: 'animate__animated animate__fadeInUp animate__faster'
            },
            hideClass: {
                popup: 'animate__animated animate__fadeOutUp animate__faster'
            },
            showCancelButton: true,
            confirmButtonColor: 'rgb(189, 151, 98)',
            cancelButtonColor: 'rgb(115, 115, 115)',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.isConfirmed) {
                Swal.fire({
                    icon: 'success',
                    timer: 1000,
                    color: 'white',
                    background: 'rgb(125, 125, 125)',
                    backdrop: `url("${images[1]}") right bottom no-repeat`,
                    showClass: {
                        popup: 'animate__animated animate__fadeInUp animate__faster'
                    },
                    hideClass: {
                        popup: 'animate__animated animate__fadeOutUp animate__faster'
                    }
                })
                history.push("/logout")
            }
        })
    }

    return (
        <Fragment>
            {
                (user.isAdmin !== true) ?
                    <Navbar expand="lg" className="w-100 border-bottom">
                        <Container className="h-100 w-100">
                            <Navbar.Brand className="text-white" as={NavLink} to="/">EMINA<small>MISE</small></Navbar.Brand>
                            <Navbar.Toggle className="text-white" aria-controls="basic-navbar-nav">Menu</Navbar.Toggle>
                            <Navbar.Toggle className="d-none" aria-controls="navbar-dark-example" hidden />
                            <Navbar.Collapse className="justify-content-lg-end justify-content-md-center m-0 p-0 navbar-collapse-background">
                                <Nav className="d-flex justify-content-lg-end justify-content-md-center m-0 p-0 h-100">
                                    <Nav.Link className="mt-3 justify-content-md-center" as={NavLink} to="/illustrations" exact>Illustrations</Nav.Link>
                                    <Nav.Link className="mt-3 justify-content-md-center" as={NavLink} to="/view-user/list" exact>Artists</Nav.Link>
                                    <Nav.Link className="mt-3 justify-content-md-center" as={NavLink} to="/checkout" exact>Cart</Nav.Link>
                                    <Nav.Link className="mt-3 mb-3 btn text-white border btn-block" as={NavLink} to="/user-sell" exact>Sell your work</Nav.Link>
                                    {
                                        (user.token !== null) ?
                                            <Fragment>
                                                <NavDropdown
                                                    id="nav-dropdown-dark-example justify-content-start"
                                                    title={
                                                        <Avatar
                                                            round={true}
                                                            size="50"
                                                            name={user.nickname}
                                                            src={localStorage.getItem('profilePictureLocal')}
                                                        />
                                                    }
                                                    menuVariant="dark"
                                                >
                                                    <NavDropdown.Item as={NavLink} to="/view-panel" exact>{user.nickname}</NavDropdown.Item>
                                                    <NavDropdown.Item as={NavLink} to="/view-my-illustrations" exact>My Illustrations</NavDropdown.Item>
                                                    <NavDropdown.Item as={NavLink} to="/order-history" exact>Order History</NavDropdown.Item>
                                                    <NavDropdown.Item as={NavLink} to="/checkout" exact>Cart/Checkout</NavDropdown.Item>
                                                    <NavDropdown.Divider />
                                                    <NavDropdown.Item onClick={(e) => logoutClick(e)}>Logout</NavDropdown.Item>
                                                </NavDropdown>
                                            </Fragment>
                                            :
                                            <Nav.Link className="mt-3" as={NavLink} to="/login" exact>Login</Nav.Link>
                                    }
                                </Nav>
                            </Navbar.Collapse>


                        </Container>
                    </Navbar>
                    :
                    <Navbar expand="lg" className="w-100 border-bottom">
                        <Container className="h-100 w-100">
                            <Navbar.Brand className="text-white" as={NavLink} to="/">EMINA<small>MISE</small></Navbar.Brand>
                            <Navbar.Toggle className="text-white" aria-controls="basic-navbar-nav">Menu</Navbar.Toggle>
                            <Navbar.Toggle className="d-none" aria-controls="navbar-dark-example" hidden />
                            <Navbar.Collapse className="justify-content-lg-end justify-content-md-center m-0 p-0 navbar-collapse-background">
                                <Nav className="d-flex justify-content-lg-end justify-content-md-center m-0 p-0 h-100">
                                    <Nav.Link className="mt-3 justify-content-md-center" as={NavLink} to="/illustrations" exact>Illustrations</Nav.Link>
                                    <Nav.Link className="mt-3 justify-content-md-center" as={NavLink} to="/admin/order-history" exact>Order Histories</Nav.Link>
                                    <Nav.Link className="mt-3 justify-content-md-center" as={NavLink} to="/admin/user-list" exact>Artist List</Nav.Link>
                                    <Nav.Link className="mt-3 justify-content-md-center" as={NavLink} to="/admin/illustration-list" exact>Illustration List</Nav.Link>
                                    {
                                        (user.token !== null) ?
                                            <Fragment>
                                                <NavDropdown
                                                    id="nav-dropdown-dark-example justify-content-start"
                                                    title={
                                                        <Avatar
                                                            round={true}
                                                            size="50"
                                                            name={user.nickname}
                                                            src={localStorage.getItem('profilePictureLocal')}
                                                        />
                                                    }
                                                    menuVariant="dark"
                                                >
                                                    <NavDropdown.Item as={NavLink} to="/view-panel" exact>{user.nickname}</NavDropdown.Item>
                                                    <NavDropdown.Divider />
                                                    <NavDropdown.Item onClick={(e) => logoutClick(e)}>Logout</NavDropdown.Item>
                                                </NavDropdown>
                                            </Fragment>
                                            :
                                            <Nav.Link className="mt-3" as={NavLink} to="/login" exact>Login</Nav.Link>
                                    }
                                </Nav>
                            </Navbar.Collapse>


                        </Container>
                    </Navbar>
            }
        </Fragment>
    )
}