import { Fragment, useContext, useState, useEffect } from 'react';
import { Container, Row, Col, Table, Button, Card } from 'react-bootstrap';
import MockData from '../MOCK_DATA.json'
import axios from 'axios'
import UserContext from '../UserContext'

export default function SortTable(props) {
    var options = { year: 'numeric', month: 'long', day: 'numeric', weekday: 'long' };
    const { user, setUser } = useContext(UserContext);
    const [data, setData] = useState(MockData)
    const [displayData, setDisplayData] = useState([])
    const [order, setOrder] = useState("ASC")
    const [orderHistory, setOrderHistory] = useState([])
    const [productsList, setProductsList] = useState([])
    const config = {
        headers: { Authorization: `Bearer ${user.token}` }
    };
    
    const getOrderHistory = async () => {
        const res = await axios.get(`https://enigmatic-inlet-99767.herokuapp.com/api/order/users/myOrders`, config);

        setProductsList(res.data)
        setOrderHistory(res.data)

        setDisplayData(
            orderHistory.map((data) => {
                let purchaseDate = formatDate(data.purchasedOn)
                return (
                    <tr key={data._id}>
                        <td className='text-white'>{purchaseDate}</td>
                        <td className='text-white'>test</td>
                        <td className='text-white'>test</td>
                    </tr>
                )
            }))
    }

    const sorting = (col) => {
        if (order === "ASC") {
            const sorted = [...data].sort((a, b) =>
                a[col].toLowerCase() > b[col].toLowerCase() ? 1 : -1
            )
            setData(sorted)
            setOrder("DSC")
        }
        if (order === "DSC") {
            const sorted = [...data].sort((a, b) =>
                a[col].toLowerCase() < b[col].toLowerCase() ? 1 : -1
            )
            setData(sorted)
            setOrder("ASC")
        }
    }

    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;

        return [year, month, day].join('-');
    }

    const getData = () => {


    }

    useEffect(() => {
        getOrderHistory()
    }, [data])
    return (
        <Container>
            <Table className="p-0 mt-5 table-dark" striped bordered hover>
                <thead>
                    <tr>
                        <th onClick={() => sorting("date")} className="text-center">Purchase Date</th>
                        <th className="text-center">Total</th>
                        <th className="text-center">Details</th>
                    </tr>
                </thead>
                <tbody>
                    {displayData}
                </tbody>
            </Table>

        </Container>
    );
}