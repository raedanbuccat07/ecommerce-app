function importAll(r) {
    return r.keys().map(r);
}

const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));


