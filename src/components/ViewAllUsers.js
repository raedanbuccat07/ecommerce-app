import { Fragment, useContext, useEffect, useState } from 'react'
import { Card, Col } from 'react-bootstrap'
import { useHistory } from 'react-router-dom'
import UserContext from '../UserContext'
import { Fade } from "react-awesome-reveal"


export default function ViewAllUsers({ usersProp, reloadList, index }) {
    const { user, setUser } = useContext(UserContext);

    const { _id, nickname, gender, profilePicture, profilePictureId } = usersProp
    const history = useHistory()
    const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));
    function importAll(r) {
        return r.keys().map(r);
    }
    const [delayTime, setDelayTime] = useState((50*index)) 

    function ViewUserProfile(e, _id) {
        e.preventDefault()
        history.push(`/view-profile/${_id}`)
    }

    useEffect(() => {

    }, [])

    return (
        <Fragment>
            <Col className="col-lg-2 p-0 m-0">
                <Fade direction='left' delay={delayTime} damping={0} triggerOnce>
                    <Card className="rounded user-card" style={{ cursor: "pointer" }} onClick={(e) => ViewUserProfile(e, _id)}>
                        <Card.Img variant="top" src={profilePicture} className="card-image-style-user illustration-thumbnail" />
                        <Card.Body className="user-card-background">
                            <Card.Title><h5 className="text-center">{nickname}</h5></Card.Title>
                        </Card.Body>
                    </Card>
                </Fade>
            </Col>
        </Fragment>
    )
}