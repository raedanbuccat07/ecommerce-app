import { Fragment, useContext, useState } from 'react'
import { Card, Col, Row } from 'react-bootstrap'
import { Link, useHistory } from 'react-router-dom'
import UserContext from '../UserContext'
import { Fade } from "react-awesome-reveal";

export default function MyIllustrations2({ illustrationProp, index }) {
    const { user, setUser } = useContext(UserContext);

    const { createdOn, description, isActive, isFeatured, likes, name, price, productPicture, sellerId, _id } = illustrationProp
    const history = useHistory()
    const [delayTime, setDelayTime] = useState((50 * index))
    function ViewIllustration(e, _id, sellerId) {
        e.preventDefault()
        localStorage.setItem("sellerIllustrationsPage", 1)
        history.push(`/illustrations/view/${_id}/${sellerId}`)
    }

    return (
        <Fragment>
            <Col className="my-3 mx-2 col-auto p-0 m-0 d-flex justify-content-center">
                <Fade direction='left' delay={delayTime} damping={0} triggerOnce>
                    <Card className="card-product" style={{ height: '35rem'}}>
                        <Card.Img
                            onClick={(e) => ViewIllustration(e, _id, sellerId)}
                            className="card-image-style rounded illustration-thumbnail"
                            variant="top"
                            src={productPicture}
                        />
                        <Card.Body className="p-0 m-0 d-flex justify-content-center">
                            <Row className="w-100 p-0 m-0">
                                <Col className="col-12 p-0 m-0 d-flex justify-content-center">
                                    <Link
                                        className="h2 h-100 w-100 card-title btn text-white text-start font-weight-bold align-middle"
                                        to={`/illustrations/view/${_id}/${sellerId}`}>{name}
                                    </Link>
                                </Col>

                            </Row>
                        </Card.Body>
                    </Card>
                </Fade>
            </Col>
        </Fragment>
    )
}