import { Fragment, useContext } from 'react'
import { Card, Col, Row} from 'react-bootstrap'
import { Link, useHistory } from 'react-router-dom'
import UserContext from '../UserContext'
import Avatar from 'react-avatar';

export default function ViewUserIllustrationCards({ illustrationProp, index }) {
    const { user, setUser } = useContext(UserContext);
    const history = useHistory()
    const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));
    function importAll(r) {
        return r.keys().map(r);
    }
    const { createdOn, description, isActive, isFeatured, likes, name, price, productPicture, profilePicture, sellerId, _id } = illustrationProp

    function test(e, _id){
        e.preventDefault()
        
        history.push(`/view-profile/${_id}`)
    }

    return (

        <Fragment>
            <Col className="my-3 mx-2 col-auto p-0 m-0 d-flex justify-content-center">
                <Card className="card-product" style={{ height: '35rem'}}>
                    <Card.Img
                        className="card-image-style rounded"
                        variant="top"
                        src={productPicture} />
                    <Card.Body className="p-0 m-0 d-flex justify-content-center">
                        <Row className="w-100 p-0 m-0 mt-3">
                            <Col className="col-4 d-flex justify-content-center p-0 m-0">
                                <Avatar
                                    round={true}
                                    size="50"
                                    src={profilePicture}
                                    onClick={ (e) => test(e, _id)}
                                    className="avatar-illustration-cards"
                                />
                            </Col>

                            <Col className="col-8 d-flex justify-content-center p-0 m-0">
                                <Link
                                    className="h4 h-50 w-100 card-title btn btn-secondary mt-2 text-white text-center"
                                    to={`illustrations/view/${_id}/${sellerId}`}>{name}
                                </Link>
                            </Col>
                        </Row>
                    </Card.Body>
                </Card>
            </Col>
        </Fragment>

    )
}