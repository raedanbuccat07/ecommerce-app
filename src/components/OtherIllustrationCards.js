import { Fragment, useContext, useState, useEffect } from 'react'
import { Card, Col, Row } from 'react-bootstrap'
import { Link, useHistory } from 'react-router-dom'
import UserContext from '../UserContext'
import Avatar from 'react-avatar';
import Heart from "react-heart"
import { Fade } from "react-awesome-reveal";

export default function OtherIllustrationCards({ illustrationProp, reloadComponent, type, index }) {
    const { user, setUser } = useContext(UserContext);
    const history = useHistory()
    const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));
    function importAll(r) {
        return r.keys().map(r);
    }
    const [delayTime, setDelayTime] = useState((50 * index))
    const [active, setActive] = useState(false)
    const [favoriteId, setFavoriteId] = useState()
    const [direction, setDirection] = useState('left')
    const { createdOn, description, isActive, isFeatured, likes, name, price, productPicture, profilePicture, sellerId, sellerData, _id, favorites } = illustrationProp

    let cardSize = "35rem"

    if (type === "featured") {
        cardSize = "80rem"
    } else {
        cardSize = "35rem"
    }

    function ViewUserProfile(e, _id) {
        e.preventDefault()
        history.push(`/view-profile/${_id}`)
    }

    function ViewIllustration(e, _id, sellerId) {
        e.preventDefault()
        localStorage.setItem("sellerIllustrationsPage", 1)
        history.push(`/illustrations/view/${_id}/${sellerId}`)
    }

    function setAsFavorite(_id) {
        if (active === false) {
            fetch(`https://enigmatic-inlet-99767.herokuapp.com/api/products/favorite`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${user.token}`
                },
                body: JSON.stringify({
                    productId: _id
                })
            }).then(() => {
                setActive(true)
            }).then(() => {
                reloadComponent(_id)
            })
        } else if (active === true) {
            fetch(`https://enigmatic-inlet-99767.herokuapp.com/api/products/unfavorite`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${user.token}`
                },
                body: JSON.stringify({
                    productId: _id,
                    userId: user.id
                })
            }).then(() => {
                setActive(false)
            }).then(() => {
                reloadComponent(_id)
            })
        }
    }

    useEffect(() => {
        if (favorites !== undefined) {
            favorites.map((data, index) => {
                if (favorites[index].userId === user.id) {
                    setActive(true)
                    setFavoriteId(data._id)
                }
            })
        } else {
            setActive(false)
        }

        if (type === "illustration") {
            setDirection("left")
        } else {
            setDirection("right")
        }
    }, [setActive])

    return (
        <Fragment>
            <Col className="my-3 mx-2 col-auto p-0 m-0 d-flex justify-content-center">
                <Fade direction={direction} delay={delayTime} damping={0} triggerOnce>
                    <Card className="card-product" style={{ height: cardSize }}>
                        <Card.Img
                            onClick={(e) => ViewIllustration(e, _id, sellerId)}
                            className="card-image-style rounded illustration-thumbnail"
                            variant="top"
                            src={productPicture} />
                        <Card.Body className="p-0 m-0 d-flex justify-content-center">
                            <Row className="w-100 p-0 m-0">
                                <Col className="col-12 p-0 m-0 d-flex justify-content-center">
                                    <Link
                                        className="h2 h-100 w-100 card-title btn text-white text-start font-weight-bold align-middle"
                                        to={`/illustrations/view/${_id}/${sellerId}`}>{name}
                                    </Link>
                                </Col>
                                {
                                    (type !== "featured") ?
                                        <Fragment>
                                            <Col
                                                onClick={(e) => ViewUserProfile(e, sellerId)}
                                                className="col-12 d-flex justify-content-center p-0 m-0 avatar-illustration-cards">
                                                <Avatar
                                                    round={true}
                                                    size="30"
                                                    src={sellerData[0].profilePicture}
                                                    className="avatar-illustration-cards"
                                                />
                                            </Col>
                                            <Col
                                                onClick={(e) => ViewUserProfile(e, sellerId)}
                                                className="col-12 d-flex justify-content-center align-items-center p-0 m-0 avatar-illustration-cards">
                                                <h5 className="text-white align-middle">{sellerData[0].nickname}</h5>
                                            </Col>
                                        </Fragment>
                                        :
                                        <Fragment>
                                            <Col
                                                onClick={(e) => ViewUserProfile(e, sellerId)}
                                                className="col-12 d-flex justify-content-center align-items-center p-0 m-0 avatar-illustration-cards">
                                                <Avatar
                                                    round={true}
                                                    size="50"
                                                    src={sellerData[0].profilePicture}
                                                    className="avatar-illustration-cards mr-5"
                                                />
                                            
                                            </Col>
                                            <Col
                                                onClick={(e) => ViewUserProfile(e, sellerId)}
                                                className="col-12 d-flex justify-content-center align-items-center p-0 m-0 avatar-illustration-cards">

                                                <h5 className="">{sellerData[0].nickname}</h5>
                                            </Col>

                                        </Fragment>
                                }


                            </Row>
                        </Card.Body>
                        {
                            (user.token !== null && user.isAdmin !== true && type !== "featured") ?
                                <Heart
                                    className={`like-button like-button-${_id}`}
                                    isActive={active}
                                    onClick={() => setAsFavorite(_id)}
                                    style={{ fill: active ? "red" : "white", stroke: active ? "red" : "gray", filter: "drop-shadow(0px 3px 3px rgba(0, 0, 0, 1))" }} />
                                :
                                <></>
                        }
                    </Card>
                </Fade>
            </Col>
        </Fragment>
    )
}