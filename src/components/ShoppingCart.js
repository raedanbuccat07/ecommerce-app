import { Fragment, useContext, useEffect } from 'react'
import { Card, Button } from 'react-bootstrap'
import UserContext from '../UserContext'

export default function ShoppingCart({ cartProp, subtotal, index, getTotal, total, reloadCart }) {
    const { user, setUser } = useContext(UserContext);

    const { ownerId, price, productId, productName, productPicture, quantity, sellerId, _id } = cartProp

    function calculateTotal(e, index, price) {
        e.preventDefault()
        let subtotal = e.target.value * price
        var x = document.getElementsByClassName(`subtotal-number${index}`)
        x[0].innerHTML = subtotal
    }

    function saveQuantity(e, index, cartId, price) {
        e.preventDefault()

        let originalInput = document.getElementById(`originalInput${index}`).value
        if (originalInput !== e.target.value) {
            let newQuantity = e.target.value
            document.getElementById(`originalInput${index}`).value = newQuantity
            let difference = newQuantity - originalInput;
            if (difference > 0) {
                total = (total + (difference * price))
            } else if (difference < 0) {
                total = (total - Math.abs(difference * price))
            }

            var x = document.getElementById('total-number-text')
            x.innerHTML = `Total: ${total}`

            fetch(`https://enigmatic-inlet-99767.herokuapp.com/api/cart/${cartId}/changeQuantity`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${user.token}`
                },
                body: JSON.stringify({
                    quantity: newQuantity
                })
            }).then(() => {
                reloadCart()
            })
        }
    }

    function removeCartItem(e, index, cartId, price) {
        e.preventDefault()

        fetch(`https://enigmatic-inlet-99767.herokuapp.com/api/cart/${cartId}/removeCartItem`, {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${user.token}`
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data == true) {
                    reloadCart()
                    let originalInput = document.getElementById(`originalInput${index}`).value
                    let priceDeduct = originalInput * price
                    total = total - priceDeduct
                    var x = document.getElementById('total-number-text')
                    x.innerHTML = `Total: ${total}`
                }
            })
    }
    useEffect(() => {
        getTotal(subtotal)
    }, [])

    return (
        <Fragment>
            <tr id={`table-row-item${index}`}>
                <td className="d-flex justify-content-center">
                    <Card className="card-product" style={{ width: '10rem', height: '10rem' }}>
                        <Card.Img
                            className="checkout-preview-image rounded"
                            variant="top"
                            src={productPicture} />
                    </Card>
                </td>
                <td className="text-center">{productName}</td>
                <td className="text-center">{price}</td>
                <td className="text-center w-25">
                    <input
                        id={`changeInput${index}`}
                        type="number"
                        onBlur={(e) => saveQuantity(e, index, _id, price)}
                        defaultValue={quantity}
                        onChange={(e) => calculateTotal(e, index, price)}
                    />
                    <input
                        id={`originalInput${index}`}
                        defaultValue={quantity} hidden
                    />
                </td>
                <td className={`text-center subtotal-number${index}`}>{subtotal}</td>
                <td className="text-center"><Button variant="danger" onClick={(e) => removeCartItem(e, index, _id, price)}>Remove</Button></td>
            </tr>
        </Fragment>
    )
}