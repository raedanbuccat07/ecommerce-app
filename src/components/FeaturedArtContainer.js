import { Container, Row, Col } from 'react-bootstrap'
import axios from 'axios'
import { useHistory } from 'react-router-dom'
import { useEffect, useState, Fragment } from 'react'
import Spinner from "react-spinners/ScaleLoader"
import OtherIllustrationCards from '../components/OtherIllustrationCards'

export default function FeaturedArtContainer() {

    const history = useHistory()
    let [loading, setLoading] = useState(true);
    let [color, setColor] = useState("#ffffff");
    const [featuredData, setFeaturedData] = useState([])

    const reloadComponent = async (_id) => {
        getFeatured()
    }

    const getFeatured = async () => {
        setLoading(true)
        await axios.get(`https://enigmatic-inlet-99767.herokuapp.com/api/products/featured`)
            .then((result) => {

                return result.data
            }).then((data) => {
                if (data !== false) {

                    setFeaturedData(
                        data.map((illustrations, index) => {
                            return (
                                <OtherIllustrationCards index={index} key={illustrations._id} illustrationProp={illustrations} reloadComponent={reloadComponent} type={"featured"} />
                            )
                        })
                    )
                    setLoading(false)
                }
            })
    }

    useEffect(() => {
        getFeatured()
    }, [])

    return (
        <Fragment>
            <Container fluid className="illustration-box">
                <Container fluid className="p-0 m-0 d-flex justify-content-center">
                    <Row className="p-0 m-0 w-100">
                        <Col className="universal-box col-lg-12 col-md-12 col-sm-12 p-0 mt-2">
                            <Row className="w-100 p-0 m-0 d-flex justify-content-center">
                                <Col className="mt-3 col-12 p-0 m-0  d-flex justify-content-center">
                                    <Spinner color={color} loading={loading} />
                                    <h1> Featured Illustrations </h1>
                                    <Spinner color={color} loading={loading} />
                                </Col>
                                {
                                    (featuredData.length !== 0) ?
                                        <Fragment>
                                            {featuredData}
                                        </Fragment>
                                        :
                                        <></>
                                }
                            </Row>
                        </Col>
                    </Row>
                </Container>
            </Container>
        </Fragment>
    )
}