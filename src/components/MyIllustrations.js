import { useState, Fragment, useContext, useEffect } from 'react'
import { Card, Col, Row, Button } from 'react-bootstrap'
import { Link, useHistory } from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import { FaTrashAlt } from 'react-icons/fa'
import { Fade } from "react-awesome-reveal"

export default function MyIllustrations({ illustrationProp, reloadList, index }) {
    const { user, setUser } = useContext(UserContext);

    const { createdOn, description, isActive, isFeatured, likes, name, price, productPicture, sellerId, _id } = illustrationProp
    const history = useHistory()
    const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));
    function importAll(r) {
        return r.keys().map(r);
    }
    const [delayTime, setDelayTime] = useState((50 * index))
    const [isActive2, setIsActive2] = useState(isActive)

    function ViewIllustration(e, _id, sellerId) {
        e.preventDefault()
        localStorage.setItem("sellerIllustrationsPage", 1)
        history.push(`/illustrations/view/${_id}/${sellerId}`)
    }

    function unarchiveProduct(e, _id) {
        e.preventDefault()
        fetch(`https://enigmatic-inlet-99767.herokuapp.com/api/products/${_id}/unarchive`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${user.token}`
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data == true) {
                    setIsActive2(true)
                    Swal.fire({
                        icon: 'success',
                        timer: 1000,
                        color: 'white',
                        background: 'rgb(125, 125, 125)',
                        backdrop: `url("${images[1]}") right bottom no-repeat`,
                        showClass: {
                            popup: 'animate__animated animate__fadeInUp animate__faster'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutUp animate__faster'
                        }
                    })

                    reloadList()
                }
            })

    }

    useEffect(() => {

    }, [])

    function archiveProduct(e, _id) {
        e.preventDefault()
        fetch(`https://enigmatic-inlet-99767.herokuapp.com/api/products/${_id}/archive`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${user.token}`
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data == true) {
                    setIsActive2(false)
                    Swal.fire({
                        icon: 'success',
                        timer: 1000,
                        color: 'white',
                        background: 'rgb(125, 125, 125)',
                        backdrop: `url("${images[1]}") right bottom no-repeat`,
                        showClass: {
                            popup: 'animate__animated animate__fadeInUp animate__faster'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutUp animate__faster'
                        }
                    })
                    reloadList()
                }
            })
    }

    function deleteProduct(e, _id, productName) {
        e.preventDefault()

        Swal.fire({
            title: `Delete ${productName}`,
            icon: "question",
            text: "Are you sure you want to delete this Illustration?",
            color: 'white',
            background: 'rgb(125, 125, 125)',
            backdrop: `url("${images[2]}") right bottom no-repeat`,
            showClass: {
                popup: 'animate__animated animate__fadeInUp animate__faster'
            },
            hideClass: {
                popup: 'animate__animated animate__fadeOutUp animate__faster'
            },
            showCancelButton: true,
            confirmButtonColor: 'rgb(189, 151, 98)',
            cancelButtonColor: 'rgb(115, 115, 115)',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.isConfirmed) {
                fetch(`https://enigmatic-inlet-99767.herokuapp.com/api/products/${_id}/delete`, {
                    method: "DELETE",
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${user.token}`
                    }
                })
                    .then(res => res.json())
                    .then(data => {
                        if (data == true) {
                            Swal.fire({
                                icon: 'success',
                                timer: 1000,
                                color: 'white',
                                background: 'rgb(125, 125, 125)',
                                backdrop: `url("${images[1]}") right bottom no-repeat`,
                                showClass: {
                                    popup: 'animate__animated animate__fadeInUp animate__faster'
                                },
                                hideClass: {
                                    popup: 'animate__animated animate__fadeOutUp animate__faster'
                                }
                            })
                            reloadList()
                        }
                    })
            }
        })
    }


    //const test = require(`../images/productPictures/${productPicture}`)



    return (
        <Fragment>

            <Col className="my-3 mx-2 col-auto p-0 m-0 d-flex justify-content-center">
                <Fade direction='left' delay={delayTime} damping={0} triggerOnce>
                    <Card className="card-product" style={{ height: '35rem'}}>
                        <Card.Img
                            onClick={(e) => ViewIllustration(e, _id, sellerId)}
                            className="card-image-style rounded illustration-thumbnail"
                            variant="top"
                            src={productPicture}
                        />
                        <Card.Body className="p-0 m-0 w-100">
                            <Row className="w-100 p-0 m-0">
                                <Col className="col-12 p-0 m-0 d-flex justify-content-center">
                                    <Link
                                        className="h2 h-100 w-100 card-title btn text-white text-start font-weight-bold align-middle"
                                        to={`/illustrations/view/${_id}/${sellerId}`}>{name}
                                    </Link>
                                </Col>

                                {
                                    (isActive2 != true) ?
                                        <Fragment>
                                            <Col className="m-0 p-0 col-10 mt-2 d-flex justify-content-center">
                                                <Button className="w-100" variant="primary" onClick={(e) => unarchiveProduct(e, _id)}>Unarchive</Button>
                                            </Col>
                                            <Col className="m-0 p-0 col-2 mt-2 d-flex justify-content-center">
                                                <Button className="w-100" variant="danger" onClick={(e) => deleteProduct(e, _id, name)}><FaTrashAlt /></Button>
                                            </Col>
                                        </Fragment>
                                        :
                                        <Fragment>
                                            <Col className="m-0 p-0 col-10 mt-2 d-flex justify-content-center">
                                                <Button className="w-100" variant="secondary" onClick={(e) => archiveProduct(e, _id)}>Archive</Button>
                                            </Col>
                                            <Col className="m-0 p-0 col-2 mt-2 d-flex justify-content-center">
                                                <Button className="w-100" variant="danger" onClick={(e) => deleteProduct(e, _id, name)}><FaTrashAlt /></Button>
                                            </Col>
                                        </Fragment>
                                }
                            </Row>
                        </Card.Body>
                    </Card>
                </Fade>
            </Col>
        </Fragment>
    )
}