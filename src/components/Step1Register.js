import React, { Fragment } from "react";
import { Form, Button, Container, Row, Col, Navbar } from "react-bootstrap";
import { NavLink, Link } from 'react-router-dom';
import AppNavbarLogoOnly from '../components/AppNavbarLogoOnly'
import Swal from 'sweetalert2'

// creating functional component ans getting props from app.js and destucturing them
const StepOne = ({ nextStep, handleFormData, values }) => {

    function importAll(r) {
        return r.keys().map(r);
    }
    const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));

    // after form submit validating the form data using validator
    const submitFormData = (e) => {
        e.preventDefault();

        const email = values.email;

        fetch('https://enigmatic-inlet-99767.herokuapp.com/api/users/checkUser', {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: email
            })
        })
            .then(res => res.json())
            .then(data => {
                if (data === true) {
                    Swal.fire({
                        title: "Duplicate email detected!",
                        icon: "error",
                        text: "This email address is already registered.",
                        confirmButtonColor: 'rgb(189, 151, 98)',
                        color: 'white',
                        background: 'rgb(125, 125, 125)',
                        backdrop: `url("${images[0]}") right bottom no-repeat`,
                        showClass: {
                            popup: 'animate__animated animate__fadeInUp animate__faster'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutUp animate__faster'
                        }
                    })
                } else {
                    nextStep();
                }

            })
    };

    return (
        <Fragment>
            <AppNavbarLogoOnly />
            <Container className="register-container">
                <Row className="h-100 d-flex align-items-center justify-content-center p-0 m-0">
                    <Col className="register-box col-md-4 col-sm-12 d-flex p-0 m-0">
                        <Row className="w-100 p-0 m-0">
                            <Col className="col-12 d-flex justify-content-center align-items-center">
                                <Navbar.Brand className="text-white register-box-logo d-flex justify-content-center align-items-center" as={NavLink} to="/" exact>EMINA<small>MISE</small></Navbar.Brand>
                            </Col>
                            <Col className="col-12 d-flex justify-content-center">
                                <Row className="w-100 h-100 p-0 m-0">
                                    <h4 className="text-center">Create an Account</h4>
                                    <Col className="col-12">
                                        <Form onSubmit={submitFormData}>
                                            <Form.Group className="mb-3">
                                                <Form.Control
                                                    name="email"
                                                    defaultValue={values.email}
                                                    type="email"
                                                    placeholder="Email Address"
                                                    onChange={handleFormData("email")}
                                                    required
                                                />
                                            </Form.Group>
                                            <Form.Group className="mb-3">
                                                <Form.Control
                                                    name="password"
                                                    defaultValue={values.password}
                                                    type="password"
                                                    placeholder="Password (Min. 8 characters)"
                                                    onChange={handleFormData("password")}
                                                    minLength="8"
                                                    required
                                                />
                                            </Form.Group>
                                            <Button variant="primary btn-login border-bottom w-100 h-75 rounded-0" type="submit">
                                                Next
                                            </Button>
                                        </Form>
                                    </Col>
                                    <Link className="text-center text-white" as={NavLink} to="/login">Go back</Link>
                                </Row>
                            </Col>
                            <Col className="col-12 d-flex justify-content-center align-items-center border-top">
                                <p className="text-white disclaimer-text">
                                    Ecommerce test project made with REACT JS
                                </p>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </Fragment>
    );
};

export default StepOne;