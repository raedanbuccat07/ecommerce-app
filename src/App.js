import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Fragment, useState, useEffect } from 'react'
import Home from './pages/Home'
import Login from './pages/Login'
import Logout from './pages/Logout'
import LoginAccount from './pages/LoginAccount'
import Register from './pages/Register'
import BGSlider from './components/BGSlider'
import { UserProvider } from './UserContext'
import Illustrations from './pages/Illustrations'
import IllustrationsView from './pages/IllustrationsView'
import ViewUser from './pages/ViewUser'
import Checkout from './pages/Checkout'
import About from './pages/About'
import ViewUserList from './pages/ViewUserList'
import ViewPanel from './pages/ViewPanel'
import ViewMyIllustrations from './pages/ViewMyIllustrations'
import SellYourWork from './pages/SellYourWork'
import OrderHistory from './pages/OrderHistory'
import OrderHistoryAdmin from './pages/OrderHistoryAdmin'
import UserListAdmin from './pages/UserListAdmin'
import IllustrationListAdmin from './pages/IllustrationListAdmin'
import axios from 'axios'
import { Scrollbars } from 'react-custom-scrollbars-2'

function App() {
  // State hook for the user state that's defined for a global scope
  // Initialize an Object with properties from the localStorage
  const [user, setUser] = useState({
    id: null,
    email: null,
    nickname: null,
    gender: null,
    profilePicture: null,
    isAdmin: null,
    token: localStorage.getItem('token'),
    profilePictureLocal: localStorage.getItem('profilePictureLocal'),
    profilePictureIdLocal: localStorage.getItem('profilePictureIdLocal'),
    profilePictureId: null
  })

  //function for clearing localStorage on Logout
  const unsetUser = () => {
    localStorage.clear()
  }

  const retrieveUserDetails = async (currentPage) => {
    if (user.token !== null) {
      try {
        const config = {
          headers: { Authorization: `Bearer ${user.token}` }
        }

        const res = await axios.get(
          `https://enigmatic-inlet-99767.herokuapp.com/api/users/user/details`, config
        )
        const data = res.data;

        return data;
      } catch (error) {

      }
    }
  }

  const checkLogin = async () => {

    let formData = await retrieveUserDetails()

    await setUser({
      id: formData._id,
      email: formData.email,
      nickname: formData.nickname,
      gender: formData.gender,
      profilePictureId: formData.profilePictureId,
      profilePicture: formData.profilePicture,
      isAdmin: formData.isAdmin,
      token: localStorage.getItem('token')
    })
    localStorage.setItem("profilePictureLocal", formData.profilePicture)
    localStorage.setItem("profilePictureIdLocal", formData.profilePictureId)
    localStorage.setItem("accountId", formData._id)
  }

  useEffect(() => {
    if (localStorage.getItem('token') !== null) {
      checkLogin()
    }
  }, [])

  return (
    <Fragment>
      <Scrollbars
        autoHide
        autoHideTimeout={1000}
        autoHideDuration={200}
        renderTrackVertical={({ style, ...props }) =>
          <div {...props} style={{ ...style, backgroundColor: 'black', right: '2px', bottom: '2px', top: '2px', borderRadius: '3px', width: '5px' }} />
        }
        renderThumbVertical={({ style, ...props }) =>
          <div {...props} style={{ ...style, width: '3px', borderRadius: '4px', boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.16)', backgroundColor: '#be913d' }} />
        }>
        <UserProvider value={{ user, setUser, unsetUser }}>
          <Router>
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/login" component={Login} />
              <Route exact path="/logout" component={Logout} />
              <Route exact path="/loginAccount" component={LoginAccount} />
              <Route exact path="/register" component={Register} />
              <Route exact path="/illustrations" component={Illustrations} />
              <Route exact path="/illustrations/view/:illustrationId/:sellersId" component={IllustrationsView} />
              <Route exact path="/user-sell" component={SellYourWork} />
              <Route exact path="/checkout" component={Checkout} />
              <Route exact path="/view-my-illustrations" component={ViewMyIllustrations} />
              <Route exact path="/order-history" component={OrderHistory} />
              <Route exact path="/view-user/list" component={ViewUserList} />
              <Route exact path="/view-profile/:userId" component={ViewUser} />
              <Route exact path="/about" component={About} />
              <Route exact path="/view-panel" component={ViewPanel} />
              <Route exact path="/admin/order-history" component={OrderHistoryAdmin} />
              <Route exact path="/admin/user-list" component={UserListAdmin} />
              <Route exact path="/admin/illustration-list" component={IllustrationListAdmin} />
              <Route component={Error} />
            </Switch>
          </Router>
        </UserProvider>
      </Scrollbars>
      <BGSlider />
    </Fragment>

  );
}
export default App
