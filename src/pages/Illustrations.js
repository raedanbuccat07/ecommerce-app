import { Fragment, useEffect, useContext, useState } from 'react';
import { Container, Row, Col } from 'react-bootstrap';
import AppNavbar from '../components/AppNavbar'
import ReactPaginate from 'react-paginate'
import OtherIllustrationCards from '../components/OtherIllustrationCards'
import MyIllustrations2 from '../components/MyIllustrations2'
import FeaturedArtContainer from '../components/FeaturedArtContainer'
import axios from 'axios'
import UserContext from '../UserContext';
import Spinner from "react-spinners/ScaleLoader";

export default function Illustrations() {

	let [loadingOther, setLoadingOther] = useState(true);
	let [loadingFavorite, setLoadingFavorite] = useState(true);
	let [loadingOwn, setLoadingOwn] = useState(true);
	let [color, setColor] = useState("#ffffff");
	//<Spinner color={color} loading={true} />

	const { user, setUser } = useContext(UserContext);
	const [illustrations, setIllustrations] = useState([])
	const [myIllustrations, setMyIllustrations] = useState([])
	const [favoriteIllustrations, setFavoriteIllustrations] = useState([])
	const [pageCount, setpageCount] = useState(localStorage.getItem("otherIllustrationsPage"))
	const [pageCount2, setpageCount2] = useState(localStorage.getItem("myIllustrationsPage"))
	const [pageCount3, setpageCount3] = useState(localStorage.getItem("favoriteIllustrationsPage"))
	const [forceReload, setForceReload] = useState()
	let limitPage = 10
	const config = {
		headers: { Authorization: `Bearer ${user.token}` }
	}

	const reloadComponent = async (_id) => {
		getIllustrations().then(() => {
			getFavoriteIllustrations()
		}).then(() => {
			setForceReload(_id)
		})
	}

	const resetPage = async (mode) => {
		switch (mode) {
			case "OTHER":
				localStorage.setItem("otherIllustrationsPage", 1)
				getIllustrations()
				break
			case "FAVORITE":
				localStorage.setItem("favoriteIllustrationsPage", 1)
				getFavoriteIllustrations()
				break
			case "OWN":
				localStorage.setItem("myIllustrationsPage", 1)
				getMyIllustrations()
				break
		}
	}

	const getIllustrations = async () => {

		if (localStorage.getItem("otherIllustrationsPage") === null) {
			localStorage.setItem("otherIllustrationsPage", 1)
		}

		setLoadingOther(true)

		if (user.token !== null) {
			await axios.get(`https://enigmatic-inlet-99767.herokuapp.com/api/products/page/other/${localStorage.getItem("otherIllustrationsPage")}`, config)
				.then((res) => {
					return res.data
				}).then((data) => {
					if (data !== false) {
						const totalRes = data.pop()
						const total = totalRes.totalAllResult;

						if (localStorage.getItem("otherIllustrationsPage") > (Math.ceil(total / limitPage))) {
							resetPage("OTHER")
						}

						setpageCount(Math.ceil(total / limitPage))


						setIllustrations(
							data.map((illustrations, index) => {
								return (
									<OtherIllustrationCards index={index} key={illustrations._id} illustrationProp={illustrations} reloadComponent={reloadComponent} type={"illustration"} />
								)
							})
						)
						setLoadingOther(false)

					}
				})
		} else {
			const res = await axios.get(`https://enigmatic-inlet-99767.herokuapp.com/api/products/page/${localStorage.getItem("otherIllustrationsPage")}`, config);
			const totalRes = res.data.pop()
			const total = totalRes.totalAllResult;

			if (localStorage.getItem("otherIllustrationsPage") > (Math.ceil(total / limitPage))) {
				resetPage("OTHER")
			}

			setpageCount(Math.ceil(total / limitPage));
			setIllustrations(
				res.data.map((illustrations) => {
					return (
						<OtherIllustrationCards key={illustrations._id} illustrationProp={illustrations} reloadComponent={reloadComponent} type={"illustration"} />
					)
				})
			)
			setLoadingOther(false)
		}
	}

	const getFavoriteIllustrations = async () => {
		if (localStorage.getItem("favoriteIllustrationsPage") === null) {
			localStorage.setItem("favoriteIllustrationsPage", 1)
		}
		setLoadingFavorite(true)
		if (user.token !== null) {
			const res = await axios.get(`https://enigmatic-inlet-99767.herokuapp.com/api/products/page/other/${localStorage.getItem("favoriteIllustrationsPage")}/favorite`, config);
			if (res.data !== false) {
				const totalRes = res.data.pop()
				const total = totalRes.totalAllResult;

				if (localStorage.getItem("favoriteIllustrationsPage") > (Math.ceil(total / limitPage))) {
					resetPage("FAVORITE")
				}

				setpageCount3(Math.ceil(total / limitPage));
				setFavoriteIllustrations(
					res.data.map((illustrations, index) => {
						return (
							<OtherIllustrationCards index={index} key={illustrations._id} illustrationProp={illustrations} reloadComponent={reloadComponent} type={"favorite"} />
						)
					})
				)
				setLoadingFavorite(false)
			}
		}
	}

	const getMyIllustrations = async () => {
		setLoadingOwn(true)
		if (user.token !== null) {
			if (localStorage.getItem("myIllustrationsPage") === null) {
				localStorage.setItem("myIllustrationsPage", 1)
			}
			const res = await axios.get(`https://enigmatic-inlet-99767.herokuapp.com/api/products/page/other/own/${localStorage.getItem("myIllustrationsPage")}`, config);
			const totalRes = res.data.pop()
			const total = totalRes.totalAllResult;

			if (localStorage.getItem("myIllustrationsPage") > (Math.ceil(total / limitPage))) {
				resetPage("OWN")
			}

			setpageCount2(Math.ceil(total / limitPage));
			setMyIllustrations(
				res.data.map((illustrations, index) => {
					return (
						<MyIllustrations2 index={index} key={illustrations._id} illustrationProp={illustrations} />
					)
				})
			)
			setLoadingOwn(false)
		}
	}

	const fetchIllustrations = async (currentPage) => {
		if (user.token !== null) {
			const res = await axios.get(
				`https://enigmatic-inlet-99767.herokuapp.com/api/products/page/other/${currentPage}`, config
			);
			const data = res.data;
			return data;
		} else {
			const res = await axios.get(
				`https://enigmatic-inlet-99767.herokuapp.com/api/products/page/${currentPage}`, config
			);
			const data = res.data;
			return data;
		}
	}

	const fetchFavoriteIllustrations = async (currentPage) => {
		if (user.token !== null) {
			const res = await axios.get(
				`https://enigmatic-inlet-99767.herokuapp.com/api/products/page/other/${currentPage}/favorite`, config
			);
			const data = res.data;
			return data;
		}
	}

	const fetchMyIllustrations = async (currentPage) => {
		if (user.token !== null) {
			const res = await axios.get(
				`https://enigmatic-inlet-99767.herokuapp.com/api/products/page/other/own/${currentPage}`, config
			);
			const data = res.data;
			return data;
		}
	}

	const handlePageClick = async (data) => {
		setLoadingOther(true)
		let currentPage = data.selected + 1
		let illustrationsFormServer = ""
		localStorage.setItem("otherIllustrationsPage", currentPage)
		illustrationsFormServer = await fetchIllustrations(currentPage)
		illustrationsFormServer.pop()
		setIllustrations(
			illustrationsFormServer.map((illustrations, index) => {
				return (
					<OtherIllustrationCards index={index} key={illustrations._id} illustrationProp={illustrations} reloadComponent={reloadComponent} type={"illustration"} />
				)
			})
		)
		setLoadingOther(false)
	}

	const handlePageClick2 = async (data) => {
		setLoadingOwn(true)

		let currentPage = data.selected + 1
		let illustrationsFormServer = ""
		localStorage.setItem("myIllustrationsPage", currentPage)
		illustrationsFormServer = await fetchMyIllustrations(currentPage)
		illustrationsFormServer.pop()
		setMyIllustrations(
			illustrationsFormServer.map((illustrations, index) => {
				return (
					<MyIllustrations2 index={index} key={illustrations._id} illustrationProp={illustrations} reloadComponent={reloadComponent} type={"favorite"} />
				)
			})
		)
		setLoadingOwn(false)

	}

	const handlePageClick3 = async (data) => {
		setLoadingFavorite(true)
		let currentPage = data.selected + 1
		let illustrationsFormServer = ""
		localStorage.setItem("favoriteIllustrationsPage", currentPage)
		illustrationsFormServer = await fetchFavoriteIllustrations(currentPage)
		illustrationsFormServer.pop()
		setFavoriteIllustrations(
			illustrationsFormServer.map((illustrations, index) => {
				return (
					<OtherIllustrationCards index={index} key={illustrations._id} illustrationProp={illustrations} />
				)
			})
		)
		setLoadingFavorite(false)
	}

	useEffect(() => {
		getIllustrations().then(() => {
			getFavoriteIllustrations()
		}).then(() => {
			getMyIllustrations()
		})

	}, [forceReload])

	return (
		<Fragment>
			<AppNavbar />
			<Container fluid className="illustration-box">
				<Container fluid className="p-0 m-0 d-flex justify-content-center">
					<Row className="p-0 m-0 w-100">
						<Col className="universal-box col-lg-12 col-md-12 col-sm-12 p-0 mt-2">
							<Row className="w-100 p-0 m-0 d-flex justify-content-center">
								<Col className="mt-3 col-12 p-0 m-0  d-flex justify-content-center">
									<Spinner color={color} loading={loadingOther} />
									<h1> Illustration Canvas for sale by various artists </h1>
									<Spinner color={color} loading={loadingOther} />
								</Col>
								{
									(illustrations.length !== 0) ?
										<Fragment>
											{illustrations}
											<Col className="col-12">
												<ReactPaginate
													previousLabel={'<'}
													nextLabel={'>'}
													breakLabel={'...'}
													pageCount={pageCount}
													marginePagesDisplayed={3}
													pageRangeDisplayed={3}
													forcePage={(localStorage.getItem("otherIllustrationsPage") - 1)}
													onPageChange={handlePageClick}
													containerClassName={'pagination justify-content-center'}
													pageClassName={'page-item'}
													pageLinkClassName={'page-link'}
													previousClassName={'page-item'}
													previousLinkClassName={'page-link'}
													nextClassName={'page-item'}
													nextLinkClassName={'page-link'}
													breakClassName={'page-item'}
													breakLinkClassName={'page-link'}
													activeClassName={'active'}
												/>
											</Col>
										</Fragment>
										:
										<></>
								}
							</Row>
						</Col>
					</Row>
				</Container>

				{
					(favoriteIllustrations.length !== 0) ?
						<Container fluid className="p-0 m-0 d-flex justify-content-center">
							<Row className="p-0 m-0 w-100">
								<Col className="universal-box col-lg-12 col-md-12 col-sm-12 p-0 mt-2">
									<Row className="w-100 p-0 m-0 d-flex justify-content-center">

										<Fragment>
											<Col className="mt-3 col-12 p-0 m-0  d-flex justify-content-center">
												<Spinner color={color} loading={loadingFavorite} />
												<h1>Favorites</h1>
												<Spinner color={color} loading={loadingFavorite} />
											</Col>
											{favoriteIllustrations}
											<Col className="col-12">
												<ReactPaginate
													previousLabel={'<'}
													nextLabel={'>'}
													breakLabel={'...'}
													pageCount={pageCount3}
													marginePagesDisplayed={3}
													pageRangeDisplayed={3}
													forcePage={(localStorage.getItem("favoriteIllustrationsPage") - 1)}
													onPageChange={handlePageClick3}
													containerClassName={'pagination justify-content-center'}
													pageClassName={'page-item'}
													pageLinkClassName={'page-link'}
													previousClassName={'page-item'}
													previousLinkClassName={'page-link'}
													nextClassName={'page-item'}
													nextLinkClassName={'page-link'}
													breakClassName={'page-item'}
													breakLinkClassName={'page-link'}
													activeClassName={'active'}
												/>
											</Col>
										</Fragment>

									</Row>
								</Col>
							</Row>
						</Container>
						:
						<></>
				}

				{
					(myIllustrations.length !== 0) ?
						<Container fluid className="p-0 m-0 d-flex justify-content-center">
							<Row className="p-0 m-0 w-100">
								<Col className="universal-box col-lg-12 col-md-12 col-sm-12 p-0 mt-2">
									<Row className="w-100 p-0 m-0 d-flex justify-content-center">
										<Col className="mt-5 col-12 p-0 m-0  d-flex justify-content-center">
											<Spinner color={color} loading={loadingOwn} />
											<h1>Illustration Canvas made by you</h1>
											<Spinner color={color} loading={loadingOwn} />
										</Col>
										{myIllustrations}

										<Col className="col-12">
											<ReactPaginate
												previousLabel={'<'}
												nextLabel={'>'}
												breakLabel={'...'}
												pageCount={pageCount2}
												marginePagesDisplayed={3}
												pageRangeDisplayed={3}
												onPageChange={handlePageClick2}
												forcePage={(localStorage.getItem("myIllustrationsPage") - 1)}
												containerClassName={'pagination justify-content-center'}
												pageClassName={'page-item'}
												pageLinkClassName={'page-link'}
												previousClassName={'page-item'}
												previousLinkClassName={'page-link'}
												nextClassName={'page-item'}
												nextLinkClassName={'page-link'}
												breakClassName={'page-item'}
												breakLinkClassName={'page-link'}
												activeClassName={'active'}
											/>
										</Col>

									</Row>
								</Col>
							</Row>
						</Container>
						:
						<></>
				}
			</Container>
			<FeaturedArtContainer />
		</Fragment>
	)
}
