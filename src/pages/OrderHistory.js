import React, { Fragment, useState, useContext, useEffect } from 'react';
import { Container, Row, Col, Modal, CloseButton, Card, Image } from 'react-bootstrap'
import { Redirect } from 'react-router-dom'
import AppNavbar from '../components/AppNavbar'
import UserContext from '../UserContext'
import axios from 'axios'
import BootstrapTable from 'react-bootstrap-table-next'
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter'
import paginationFactory from 'react-bootstrap-table2-paginator';
import { Scrollbars } from 'react-custom-scrollbars-2';


export default function OrderHistoryAdmin() {

	const { user, setUser } = useContext(UserContext);
	//
	const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));
	function importAll(r) {
		return r.keys().map(r);
	}
	const [show, setShow] = useState(false);
	const [modalData, setModalData] = useState([])
	const [modalTableData, setModalTableData] = useState([])
	const [totalModal, setTotalModal] = useState(0)

	const [showIllustrationModal, setShowIllustrationModal] = useState(false);
	const [illustrationPreview, setIllustrationPreview] = useState([])
	function handleShowIllustration(data) {
		setIllustrationPreview(data)
		setShowIllustrationModal(true);
	}

	function handleShow(data) {
		setModalData(data)
		let totalMap = data.productsPurchased
		let total = 0
		totalMap.map((data) => {
			total = (data.quantity * data.price) + total
		})
		setTotalModal(total)
		setModalTableData(data.productsPurchased)
		setShow(true);
	}

	const previewFormatter = (data, row) => {
		return <span>
			<Row>
				<Col className="d-flex justify-content-center col-12">
					<Card className="card-product" style={{ width: '10rem', height: '10rem' }}>
						<Card.Img
							className="checkout-preview-image rounded"
							variant="top"
							src={data} />
					</Card>
				</Col>
			</Row>
		</span>
	}

	const subtotalFormatter = (data, row) => {
		let subtotal = row.price * row.quantity
		return <span>
			{subtotal}
		</span>
	}

	const headerFormat = (column, colIndex, { sortElement, filterElement }) => {
		return <Row>
			<Col className="col-12 my-2">
				<h5>{column.text} {sortElement}</h5>
			</Col>
			<Col className="col-12">
				{filterElement}
			</Col>
		</Row>
	}

	const columns = [
		{
			dataField: 'purchasedOn',
			text: 'Date',
			events: {
				onClick: (e, column, columnIndex, row, rowIndex) => handleShow(row),
			},
			headerFormatter: (headerFormat),
			headerStyle: (colum, colIndex) => {
				return {
					textAlign: 'center',
					color: 'white',
					backgroundColor: '#634a1b',
					cursor: 'pointer',
				};
			},
			style: {
				color: 'white',
				cursor: 'pointer'
			},
			filter: textFilter(),
			sort: true,
		},
		{
			dataField: 'totalAmount',
			text: 'Total Amount',
			events: {
				onClick: (e, column, columnIndex, row, rowIndex) => handleShow(row),
			},
			headerFormatter: (headerFormat),
			headerStyle: (colum, colIndex) => {
				return {
					textAlign: 'center',
					color: 'white',
					backgroundColor: '#634a1b',
					cursor: 'pointer',
				};
			},
			style: {
				color: 'white',
				cursor: 'pointer'
			},
			filter: textFilter(),
			sort: true,
		},
	]

	const columnsModal = [
		{
			dataField: 'productPicture',
			text: 'Preview',
			formatter: previewFormatter,
			headerFormatter: (headerFormat),
			headerStyle: (colum, colIndex) => {
				return {
					textAlign: 'center',
					color: 'white',
					backgroundColor: '#634a1b',
					cursor: 'pointer',
				};
			},
			style: {
				color: 'white',
				cursor: 'pointer'
			},
			filter: textFilter(),
			sort: true,
			events: {
				onClick: (e, column, columnIndex, row, rowIndex) => handleShowIllustration(row),
			},
		},
		{
			dataField: 'productName',
			text: 'Title',
			headerFormatter: (headerFormat),
			headerStyle: (colum, colIndex) => {
				return {
					textAlign: 'center',
					color: 'white',
					backgroundColor: '#634a1b',
					cursor: 'pointer',
				};
			},
			style: {
				color: 'white',
				cursor: 'pointer'
			},
			filter: textFilter(),
			sort: true,
			events: {
				onClick: (e, column, columnIndex, row, rowIndex) => handleShowIllustration(row),
			},
		},
		{
			dataField: 'price',
			text: 'Price',
			headerFormatter: (headerFormat),
			headerStyle: (colum, colIndex) => {
				return {
					textAlign: 'center',
					color: 'white',
					backgroundColor: '#634a1b',
					cursor: 'pointer',
				};
			},
			style: {
				color: 'white',
				cursor: 'pointer'
			},
			filter: textFilter(),
			sort: true,
			events: {
				onClick: (e, column, columnIndex, row, rowIndex) => handleShowIllustration(row),
			},
		},
		{
			dataField: 'quantity',
			text: 'Quantity',
			headerFormatter: (headerFormat),
			headerStyle: (colum, colIndex) => {
				return {
					textAlign: 'center',
					color: 'white',
					backgroundColor: '#634a1b',
					cursor: 'pointer',
				};
			},
			style: {
				color: 'white',
				cursor: 'pointer'
			},
			filter: textFilter(),
			sort: true,
			events: {
				onClick: (e, column, columnIndex, row, rowIndex) => handleShowIllustration(row),
			},
		},
		{
			dataField: 'subtotal',
			text: 'Sub Total',
			formatter: subtotalFormatter,
			headerFormatter: (headerFormat),
			headerStyle: (colum, colIndex) => {
				return {
					textAlign: 'center',
					color: 'white',
					backgroundColor: '#634a1b',
					cursor: 'pointer',
				};
			},
			style: {
				color: 'white',
				cursor: 'pointer'
			},
			filter: textFilter(),
			sort: true,
			events: {
				onClick: (e, column, columnIndex, row, rowIndex) => handleShowIllustration(row),
			},
		},
	]

	const defaultSorted = [{
		dataField: 'purchasedOn',
		order: 'asc'
	}]

	const defaultSortedModal = [{
		dataField: 'productName',
		order: 'asc'
	}]

	const [data, setData] = useState([])

	const getData = () => {

		const config = {
			headers: { Authorization: `Bearer ${user.token}` }
		}
		axios("https://enigmatic-inlet-99767.herokuapp.com/api/order/users/myOrders", config).then((res) => {
			setData(res.data)
		})
	}

	const options = {
		paginationSize: 4,
		pageStartIndex: 0,
		firstPageText: 'First',
		prePageText: 'Back',
		nextPageText: 'Next',
		lastPageText: 'Last',
		nextPageTitle: 'First page',
		prePageTitle: 'Pre page',
		firstPageTitle: 'Next page',
		lastPageTitle: 'Last page',
		showTotal: true,
		disablePageTitle: true,
		sizePerPageList: [{
			text: '5', value: 5
		}, {
			text: '10', value: 10
		}, {
			text: '25', value: 25
		}, {
			text: '50', value: 50
		}, {
			text: 'All', value: data.length
		}]
	}

	const options2 = {
		paginationSize: 4,
		pageStartIndex: 0,
		firstPageText: 'First',
		prePageText: 'Back',
		nextPageText: 'Next',
		lastPageText: 'Last',
		nextPageTitle: 'First page',
		prePageTitle: 'Pre page',
		firstPageTitle: 'Next page',
		lastPageTitle: 'Last page',
		showTotal: true,
		disablePageTitle: true,
		sizePerPageList: [{
			text: '5', value: 5
		}, {
			text: '10', value: 10
		}, {
			text: '25', value: 25
		}, {
			text: '50', value: 50
		}, {
			text: 'All', value: modalTableData.length
		}]
	}

	useEffect(() => {
		getData()
	}, [])

	// || user.isAdmin !== true
	return (
		(user.token == null) ?
			<Redirect to="/login" />
			:
			<Fragment>
				<AppNavbar />
				<Container fluid className="illustration-box">
					<Container fluid className="p-0 m-0 d-flex justify-content-center">
						<Row className="p-0 m-0 w-50">
							<Col className="universal-box col-lg-12 col-md-12 col-sm-12 p-0 mt-2">
								<Row className="w-100 p-0 m-0 d-flex justify-content-center">
									<Fragment>
										<Col className="mt-3 col-12 p-0 m-0 d-flex justify-content-center">
											<h1>Order Histories</h1>
										</Col>
										<Col className="mt-3 col-12 p-0 m-0">
											<BootstrapTable
												striped
												hover
												condensed
												bootstrap4
												keyField="_id"
												data={data}
												columns={columns}
												defaultSorted={defaultSorted}
												filter={filterFactory()}
												pagination={paginationFactory(options)}
											/>
										</Col>
									</Fragment>
								</Row>
							</Col>
						</Row>
					</Container>
				</Container>
				{
					(show !== false) ?
						<Modal variant="dark" show={show} fullscreen={true} onHide={() => setShow(false)} className="illustration-modal">
							<Modal.Header className="illustration-modal">
								<Modal.Title className="text-center w-100">
									<Row>
										<Col>
											<h4>{modalData._id}</h4>
										</Col>
									</Row>
								</Modal.Title>
								<CloseButton variant="white" onClick={() => setShow(false)} />
							</Modal.Header>
							<Modal.Body className="illustration-modal">
								<Row>
									<Col>
										<BootstrapTable
											striped
											hover
											condensed
											bootstrap4
											keyField="_id"
											data={modalTableData}
											columns={columnsModal}
											defaultSorted={defaultSortedModal}
											filter={filterFactory()}
											pagination={paginationFactory(options2)}
										/>
									</Col>
								</Row>
								<Row>
									<Col>
										<h4 className="text-end">Total: {totalModal}</h4>
									</Col>
								</Row>
							</Modal.Body>
						</Modal>
						:
						<></>
				}
				{
					(showIllustrationModal === true) ?
						<Modal
							show={showIllustrationModal}
							fullscreen={true}
							onHide={() => setShowIllustrationModal(false)}
							className="illustration-modal"
						>
							<Modal.Header className="illustration-modal">
								<Modal.Title className="text-center w-100">{illustrationPreview.name}</Modal.Title>

								<CloseButton variant="white" onClick={() => setShowIllustrationModal(false)} />
							</Modal.Header>
							<Modal.Body className="illustration-modal d-flex justify-content-center">
								<Container fluid className="d-flex justify-content-center">
									<Scrollbars
										renderThumbHorizontal={({ style, ...props }) =>
											<div {...props} style={{ ...style, width: '3px', borderRadius: '4px', boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.16)', backgroundColor: '#be913d' }} />
										}
										renderTrackVertical={({ style, ...props }) =>
											<div {...props} style={{ ...style, backgroundColor: 'black', right: '2px', bottom: '2px', top: '2px', borderRadius: '3px', width: '5px' }} />
										}
										renderThumbVertical={({ style, ...props }) =>
											<div {...props} style={{ ...style, width: '3px', borderRadius: '4px', boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.16)', backgroundColor: '#be913d' }} />
										}>
										<Container fluid className="d-flex justify-content-center">
											<Image className="product-picture-modal" src={illustrationPreview.productPicture} />
										</Container>

									</Scrollbars>
								</Container>
							</Modal.Body>
						</Modal>
						:
						<></>
				}
			</Fragment>
	)
}
