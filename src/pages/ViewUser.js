import { Fragment, useEffect, useState, useContext } from 'react';
import { Container, Row, Col } from 'react-bootstrap'
import AppNavbar from '../components/AppNavbar'
import { useParams } from 'react-router-dom'
import ProfileIllustrations from '../components/MyIllustrations2'
import ReactPaginate from 'react-paginate'
import ProfileUi from 'react-profile-card';
import axios from 'axios'
import UserContext from '../UserContext'
import Spinner from "react-spinners/ScaleLoader";

export default function ViewUser() {

	let [loadingIllustration, setLoadingIllustration] = useState(true);
	let [loadingIllustration2, setLoadingIllustration2] = useState(false);
	let [color, setColor] = useState("#ffffff");
	//<Spinner color={color} loading={true} />

	const { userId } = useParams()
	const { user, setUser } = useContext(UserContext);
	const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));
	function importAll(r) {
		return r.keys().map(r);
	}

	const [nickname, setNickname] = useState()
	const [gender, setGender] = useState()
	const [profilePicture, setProfilePicture] = useState(require('../images/profilePictures/default.jpg'))

	const [profileIllustrations, setProfileIllustrations] = useState([])
	const [pageCount, setpageCount] = useState(1)
	let limitPage = 10
	const config = {
		headers: { Authorization: `Bearer ${user.token}` }
	};

	const getUserDetails = async () => {
		await fetch(`https://enigmatic-inlet-99767.herokuapp.com/api/users/details/${userId}`)
			.then(res => res.json())
			.then(data => {
				setNickname(data.nickname)
				setGender(data.gender)
				setProfilePicture(data.profilePicture)
			})
	}

	const getProfileIllustrations = async () => {
		setLoadingIllustration(true)
		const res = await axios.get(`https://enigmatic-inlet-99767.herokuapp.com/api/products/page/seller/${userId}/1`, config)
		const totalRes = res.data.pop()
		const total = totalRes.totalAllResult
		setpageCount(Math.ceil(total / limitPage))
		setProfileIllustrations(
			res.data.map((illustrations, index) => {
				return (
					<ProfileIllustrations index={index} key={illustrations._id} illustrationProp={illustrations} />
				)
			})
		)
		setLoadingIllustration(false)
	}

	const fetchSellerIllustrations = async (currentPage) => {
		const res = await axios.get(
			`https://enigmatic-inlet-99767.herokuapp.com/api/products/page/seller/${userId}/${currentPage}`, config
		);
		const data = res.data;

		return data;
	}

	const handlePageClick = async (data) => {
		setLoadingIllustration2(true)
		let currentPage = data.selected + 1
		let illustrationsFormServer = ""

		illustrationsFormServer = await fetchSellerIllustrations(currentPage)
		illustrationsFormServer.pop()
		setProfileIllustrations(
			illustrationsFormServer.map((illustrations) => {
				return (
					<ProfileIllustrations key={illustrations._id} illustrationProp={illustrations} />
				)
			})
		)
		setLoadingIllustration2(false)
	}

	useEffect(() => {
		getUserDetails().then(() => {
			getProfileIllustrations()
		})
	}, [])

	return (
		<Fragment>
			<AppNavbar />

			<Container fluid className="view-profile-container p-0 mb-0">
				<Container fluid className="p-0 m-0 d-flex justify-content-center">
					<Row className="w-100 p-0 m-0">
						<Col className="col-12 d-flex justify-content-center h-25 p-0 m-0">
							<ProfileUi
								imgUrl={profilePicture}
								name={nickname}
								designation={`Artist`}
							/>
						</Col>
						<Col className="col-12 h-75 p-0 m-0 avatar-bottom-box">

						</Col>
					</Row>
				</Container>
				<Container fluid className="profile-illustration-box p-0 m-0 d-flex justify-content-center">
					<Row className="p-0 m-0 w-100">
						<Col className="h-100 universal-box col-lg-12 col-md-12 col-sm-12 p-0 m-0">
							<Row className="w-100 p-0 m-0 d-flex justify-content-center">
								{
									(loadingIllustration === true) ?
										<Col className="mt-5 col-12 p-0 m-0  d-flex justify-content-center align-items-center">
											<Spinner color={color} loading={loadingIllustration} />
										</Col>
										:
										<Fragment>
											{
												(profileIllustrations.length !== 0) ?
													<Container fluid className="profile-illustration-box p-0 m-0 d-flex justify-content-center">
														<Row className="p-0 m-0 w-100">
															<Col className="h-100 universal-box col-lg-12 col-md-12 col-sm-12 p-0 m-0">
																<Row className="w-100 p-0 m-0 d-flex justify-content-center">

																	<Col className="mt-5 col-12 p-0 m-0  d-flex justify-content-center">
																		<Spinner color={color} loading={loadingIllustration2} />
																		<h1>Other Illustration Canvas by this seller</h1>
																		<Spinner color={color} loading={loadingIllustration2} />
																	</Col>

																	{profileIllustrations}
																	<Col className="col-12">
																		<ReactPaginate
																			previousLabel={'<'}
																			nextLabel={'>'}
																			breakLabel={'...'}
																			pageCount={pageCount}
																			marginePagesDisplayed={3}
																			pageRangeDisplayed={3}
																			onPageChange={handlePageClick}
																			forcePage={0}
																			containerClassName={'pagination justify-content-center'}
																			pageClassName={'page-item'}
																			pageLinkClassName={'page-link'}
																			previousClassName={'page-item'}
																			previousLinkClassName={'page-link'}
																			nextClassName={'page-item'}
																			nextLinkClassName={'page-link'}
																			breakClassName={'page-item'}
																			breakLinkClassName={'page-link'}
																			activeClassName={'active'}
																		/>
																	</Col>
																</Row>
															</Col>
														</Row>
													</Container>
													:
													<Container fluid className="profile-illustration-box p-0 m-0 d-flex justify-content-center">
														<Row className="p-0 m-0 w-100">
															<Col className="h-100 universal-box col-lg-12 col-md-12 col-sm-12 p-0 m-0">
																<Row className="w-100 p-0 m-0 d-flex justify-content-center">

																	<Col className="mt-5 col-12 p-0 m-0  d-flex justify-content-center">
																		<h1>This user has no Illustrations yet :(</h1>
																	</Col>

																</Row>
															</Col>
														</Row>
													</Container>
											}
										</Fragment>
								}

							</Row>
						</Col>
					</Row>
				</Container>
			</Container>
		</Fragment>
	)
}
