import { Fragment, useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Table, Button, Card } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom'
import AppNavbar from '../components/AppNavbar'
import ShoppingCart from '../components/ShoppingCart'
import UserContext from '../UserContext'
import axios from 'axios'
import Swal from 'sweetalert2'

export default function Checkout() {
	const { user, setUser } = useContext(UserContext);
	const history = useHistory()
	//
	const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));
	function importAll(r) {
		return r.keys().map(r);
	}

	const [cartItems, setCartItems] = useState([])
	const [total, setTotal] = useState(0)
	const config = {
		headers: { Authorization: `Bearer ${user.token}` }
	};

	let tempTotal = 0
	
	const getTotal = async (subtotal) => {
		tempTotal = tempTotal + subtotal
		setTotal(tempTotal)
	}

	const reloadCart = async () => {
		getCart()
	}

	const getCart = async () => {
		await axios.get(`https://enigmatic-inlet-99767.herokuapp.com/api/cart`, config)
		.then((res) => {
			return res.data
		}).then((data) => {
			if (data !== false) {
				setCartItems(data)
			}
		})
	};

	function checkoutCart(e) {
		e.preventDefault()

		if (cartItems.length !== 0) {
			Swal.fire({
				title: "Checkout",
				icon: "question",
				text: "Are you sure you want to Checkout?",
				color: 'white',
				background: 'rgb(125, 125, 125)',
				backdrop: `url("${images[2]}") right bottom no-repeat`,
				showClass: {
					popup: 'animate__animated animate__fadeInUp animate__faster'
				},
				hideClass: {
					popup: 'animate__animated animate__fadeOutUp animate__faster'
				},
				showCancelButton: true,
				confirmButtonColor: 'rgb(189, 151, 98)',
				cancelButtonColor: 'rgb(115, 115, 115)',
				confirmButtonText: 'Confirm'
			}).then((result) => {
				if (result.isConfirmed) {
					fetch(`https://enigmatic-inlet-99767.herokuapp.com/api/order/users/checkout`, {
						method: "POST",
						headers: {
							"Content-Type": "application/json",
							Authorization: `Bearer ${user.token}`
						}
					})
						.then(res => res.json())
						.then(data => {
							if (data == true) {
								Swal.fire({
									icon: 'success',
									timer: 1000,
									color: 'white',
									background: 'rgb(125, 125, 125)',
									backdrop: `url("${images[1]}") right bottom no-repeat`,
									showClass: {
										popup: 'animate__animated animate__fadeInUp animate__faster'
									},
									hideClass: {
										popup: 'animate__animated animate__fadeOutUp animate__faster'
									}
								})
								history.push("/order-history")
							}
						})
				}
			})
		} else {
			Swal.fire({
				title: "Empty Cart",
				icon: "error",
				text: "Please add items on the cart before checking out.",
				confirmButtonColor: 'rgb(189, 151, 98)',
				color: 'white',
				background: 'rgb(125, 125, 125)',
				backdrop: `url("${images[0]}") right bottom no-repeat`,
				showClass: {
					popup: 'animate__animated animate__fadeInUp animate__faster'
				},
				hideClass: {
					popup: 'animate__animated animate__fadeOutUp animate__faster'
				}
			})
		}
	}

	useEffect(() => {
		getCart()
	}, [])

	return (
		(user.token == null || user.token == undefined || user.isAdmin === true) ?
			<Redirect to="/login" />
			:
			<Fragment>
				<AppNavbar />
				<Container fluid className="universal-container">
					<Row className="d-flex justify-content-center ">
						<Col className="col-12">
							<h1 className="text-center mt-5">Your shopping cart</h1>
						</Col>
						<Col className="col-6 border">
							<div className="table-responsive">


								<Table className="p-0 m-0 table-dark" striped bordered hover>
									<thead>
										<tr>
											<th className="text-center">Preview Image</th>
											<th className="text-center">Name</th>
											<th className="text-center">Price</th>
											<th className="text-center">Quantity</th>
											<th className="text-center">Subtotal</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										{
											(cartItems.length !== 0) ?
												<Fragment>
													{	
														
														cartItems.map((cart, index) => {
															let subtotal = cart.price * cart.quantity
															return (
																<ShoppingCart reloadCart={reloadCart} key={cart._id} cartProp={cart} index={index} subtotal={subtotal} getTotal={getTotal} total={total}/>
															)
														})
													}
												</Fragment>
												:
												<tr>
													<td colSpan="6" className="text-center text-center w-100 checkout-button">
														<h1>Empty Cart</h1>
													</td>
												</tr>
										}
									</tbody>
									<tfoot>
										<tr>
											<td colSpan="4" className="text-center text-center w-100 checkout-button">

												<Button variant="success" className="w-100" onClick={(e) => checkoutCart(e)}>Checkout</Button>
											</td>
											<td colSpan="2" id="total-number-text">Total: {total}</td>
										</tr>
									</tfoot>
								</Table>
							</div>
						</Col>
					</Row>
				</Container>
			</Fragment>
	)
}
