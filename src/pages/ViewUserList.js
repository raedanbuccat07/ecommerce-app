import { Fragment, useState, useEffect } from 'react';
import { Container, Row, Col, CardGroup } from 'react-bootstrap';
import AppNavbar from '../components/AppNavbar'
import ViewAllUsers from '../components/ViewAllUsers'
import axios from 'axios'
import ReactPaginate from 'react-paginate'
import Spinner from "react-spinners/ScaleLoader";

export default function ViewUserList() {

	let [loadingUser, setLoadingUser] = useState(true);
	let [color, setColor] = useState("#ffffff");
	//<Spinner color={color} loading={true} />

	const [allUsers, setAllUsers] = useState([])
	const [pageCount, setpageCount] = useState(localStorage.getItem("viewArtistsPage"))
	let limitPage = 20

	const reloadList = () => {
		getAllUsers()
	}

	const resetPage = async (mode) => {
		switch (mode) {
			case "VIEW":
				localStorage.setItem("otherIllustrationsPage", 1)
				getAllUsers()
				break
		}
	}

	const getAllUsers = async () => {
		setLoadingUser(true)
		if (localStorage.getItem("viewArtistsPage") === null) {
			localStorage.setItem("viewArtistsPage", 1)
		}
		const res = await axios.get(`https://enigmatic-inlet-99767.herokuapp.com/api/users/${localStorage.getItem("viewArtistsPage")}/all`);
		const totalRes = res.data.pop()
		const total = totalRes.totalAllResult

		if (localStorage.getItem("viewArtistsPage") > (Math.ceil(total / limitPage))) {
			resetPage("VIEW")
		}

		let test = []

		setpageCount(Math.ceil(total / limitPage))
		setAllUsers(
			res.data.map((users, index) => {
				return <ViewAllUsers key={users._id} usersProp={users} reloadList={reloadList} index={index}/>
			})
		)
		setLoadingUser(false)
	}

	const fetchAllUsers = async (currentPage) => {
		const res = await axios.get(`https://enigmatic-inlet-99767.herokuapp.com/api/users/${currentPage}/all`);
		const data = res.data;
		return data;
	}

	const handlePageClick = async (data) => {

		let currentPage = data.selected + 1
		let FormServer = ""
		localStorage.setItem("viewArtistsPage", currentPage)

		FormServer = await fetchAllUsers(currentPage)
		FormServer.pop()
		setAllUsers(
			FormServer.data.map((users,index) => {
				return (
					<ViewAllUsers key={users._id} usersProp={users} reloadList={reloadList} index={index}/>
				)
			})
		)
	}

	useEffect(() => {
		getAllUsers()
	}, [])

	return (
		<Fragment>
			<AppNavbar />
			<Container fluid className="illustration-box">
				<Container fluid className="p-0 m-0 d-flex justify-content-center">
					<Row className="p-0 m-0 w-100">
						<Col className="col-12 p-0 mt-5 d-flex justify-content-center">
							<h1>Artists</h1>
						</Col>
						{
							(loadingUser === true) ?
								<Container fluid className="illustration-box d-flex justify-content-center align-items-center">
									<Spinner color={color} loading={loadingUser} size={500} />
								</Container>
								:
								<Fragment>
									{
										(allUsers.length !== 0) ?
											<Col className="universal-box col-lg-12 col-md-12 col-sm-12 p-0 mt-2">
												<Row className="w-100 p-0 m-0 d-flex justify-content-center">
													<Fragment>

														{allUsers}


														<Col className="col-12 mt-5">
															<ReactPaginate
																previousLabel={'<'}
																nextLabel={'>'}
																breakLabel={'...'}
																pageCount={pageCount}
																marginePagesDisplayed={3}
																pageRangeDisplayed={3}
																forcePage={(localStorage.getItem("viewArtistsPage") - 1)}
																onPageChange={handlePageClick}
																containerClassName={'pagination justify-content-center'}
																pageClassName={'page-item'}
																pageLinkClassName={'page-link'}
																previousClassName={'page-item'}
																previousLinkClassName={'page-link'}
																nextClassName={'page-item'}
																nextLinkClassName={'page-link'}
																breakClassName={'page-item'}
																breakLinkClassName={'page-link'}
																activeClassName={'active'}
															/>
														</Col>
													</Fragment>
												</Row>
											</Col>
											:
											<Container fluid className="illustration-box d-flex justify-content-center align-items-center">
												<h1>No users have registered yet</h1>
											</Container>
									}
								</Fragment>
						}
					</Row>
				</Container>
			</Container>
		</Fragment>
	)
}
