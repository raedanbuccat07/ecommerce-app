import React, { Fragment, useState, useContext, useEffect } from 'react';
import { Form, Container, Row, Col, Table, CloseButton, Button, Modal, ButtonGroup, ToggleButton } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import AppNavbar from '../components/AppNavbar'
import UserContext from '../UserContext'
import axios from 'axios'
import AvatarEditor from 'react-avatar-editor'
import Swal from 'sweetalert2'
import DefaultImage from '../images/profilePictures/default.jpg'

export default function ViewPanel() {
	const { user, setUser } = useContext(UserContext);

	const [removeProfilePicture, setRemoveProfilePicture] = useState(false)
	const [disableSubmit, setDisableSubmit] = useState(false)

	//
	const [nickname, setNickname] = useState(user.nickname)
	const [profilePicture, setProfilePicture] = useState(localStorage.getItem('profilePictureLocal'))
	const [profilePictureId, setProfilePictureId] = useState(localStorage.getItem('profilePictureIdLocal'))
	const [gender, setGender] = useState(localStorage.getItem("gender"))
	const [email, setEmail] = useState(user.email)
	const [newPassword, setNewPassword] = useState(null)
	//
	const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));
	function importAll(r) {
		return r.keys().map(r);
	}

	const radios = [
		{ name: 'Male', value: 'Male' },
		{ name: 'Female', value: 'Female' },
		{ name: 'Other', value: 'Other' },
	];

	const [image, setImage] = useState()
	const [allowZoomOut, setAllowZoomOut] = useState(false)
	const [position, setPosition] = useState({ x: 0.5, y: 0.5 })
	const [scale, setScale] = useState(1)
	const [rotate, setRotate] = useState(0)
	const [width, setWidth] = useState(200)
	const [height, setHeight] = useState(200)
	const [modalShow, setModalShow] = React.useState(false);
	const [isImageSelected, setIsImageSelected] = useState(false)
	const [editor, setEditor] = useState(null)
	const [previewImage, setPreviewImage] = useState()
	const [blobImage, setBlobImage] = useState(null)
	const setEditorRef = (editor) => (setEditor(editor))

	//ILLUST
	const handleNewImage = e => {
		setImage(e.target.files[0])
		setIsImageSelected(true)
		setModalShow(true)
	}

	const handleScale = e => {
		const scale2 = parseFloat(e.target.value)
		setScale(scale2)
	}

	const handlePositionChange = position => {
		setPosition(position)
	}

	function SaveChanges(e) {
		e.preventDefault()
		if (editor) {
			const url = editor.getImageScaledToCanvas().toDataURL();
			setBlobImage(editor.getImageScaledToCanvas())
			setPreviewImage(url)
		}
		setRemoveProfilePicture(false)
		setIsImageSelected(true)
		setModalShow(false)
	}

	function CancelChanges(e) {
		e.preventDefault()
		setImage()
		setIsImageSelected(false)
		document.getElementById("avatar-photo").value = ""
		setModalShow(false)
	}

	function CloseModal(e) {
		e.preventDefault()
		setModalShow(false)
	}

	function resetAvatar(e) {
		e.preventDefault()
		setRemoveProfilePicture(false)
		setImage(localStorage.getItem('profilePictureLocal'))
		setIsImageSelected(false)
		document.getElementById("avatar-photo").value = ""
		setModalShow(false)
	}

	function removeAvatar(e) {
		e.preventDefault()
		setRemoveProfilePicture(true)
		setImage(DefaultImage)
		setIsImageSelected(true)
		setPreviewImage(DefaultImage)
		document.getElementById("avatar-photo").value = ""
		setModalShow(false)
	}

	const blobToBase64 = async (blob) => {
		return new Promise((resolve, _) => {
			const reader = new FileReader();
			reader.onloadend = () => resolve(reader.result);
			reader.readAsDataURL(blob);
		});
	}

	const getBase64FromUrl = async (url) => {
		const data = await fetch(url);
		const blob = await data.blob();
		return new Promise((resolve) => {
			const reader = new FileReader();
			reader.readAsDataURL(blob);
			reader.onloadend = () => {
				const base64data = reader.result;
				resolve(base64data);
			}
		});
	}

	async function updateUser(e) {
		e.preventDefault()

		if (user.email != email && email != undefined && email != null) {
			setUser({
				id: user.id,
				email: email,
				nickname: user.nickname,
				gender: user.gender,
				profilePicture: localStorage.getItem('profilePictureLocal'),
				isAdmin: user.isAdmin,
				token: localStorage.getItem('token'),
				profilePictureLocal: localStorage.getItem('profilePictureLocal')
			})
		}

		if (user.nickname != nickname && nickname != undefined && nickname != null) {
			setUser({
				id: user.id,
				email: user.email,
				nickname: nickname,
				gender: user.gender,
				profilePicture: localStorage.getItem('profilePictureLocal'),
				isAdmin: user.isAdmin,
				token: localStorage.getItem('token'),
				profilePictureLocal: localStorage.getItem('profilePictureLocal')
			})

		}
		if (gender != user.gender && gender != undefined && gender != null) {
			setUser({
				id: user.id,
				email: user.email,
				nickname: user.nickname,
				gender: gender,
				profilePicture: localStorage.getItem('profilePictureLocal'),
				isAdmin: user.isAdmin,
				token: localStorage.getItem('token'),
				profilePictureLocal: localStorage.getItem('profilePictureLocal')
			})
		}

		let fileImage = ""

		if (blobImage !== null) {
			blobImage.toBlob(async function (blob) {
				fileImage = await blobToBase64(blob)
				axiosUpdate(fileImage)
			})
		}
		else if (removeProfilePicture === true) {
			fileImage = await getBase64FromUrl(DefaultImage)
			axiosUpdate(fileImage)
		}
		else {
			fileImage = await profilePicture
			axiosUpdate(fileImage)
		}
	}

	const axiosUpdate = async (image) => {

		const config = {
			headers: { Authorization: `Bearer ${user.token}`, "Content-Type": "application/json" }
		};

		let emailUpdate = email
		let nicknameUpdate = nickname


		if (email === null || email === undefined) {
			emailUpdate = user.email
		}
		if (nickname === null || nickname === undefined) {
			nicknameUpdate = user.nickname
		}

		setDisableSubmit(true)
		await axios.put('https://enigmatic-inlet-99767.herokuapp.com/api/users/update', {
			email: emailUpdate,
			password: newPassword,
			nickname: nicknameUpdate,
			gender: gender,
			file: image,
			profilePictureId: profilePictureId
		}, config)
			.then((response) => {
				if (response.data === true) {
					setDisableSubmit(false)
					setIsImageSelected(false)
					setRemoveProfilePicture(false)
					localStorage.setItem("gender", gender)
					localStorage.setItem("profilePictureLocal", image)
					document.getElementById("avatar-photo").value = ""
					setProfilePicture(localStorage.getItem('profilePictureLocal'))
					Swal.fire({
						title: "Updated!",
						icon: "success",
						text: "You have successfully updated your details.",
						confirmButtonColor: 'rgb(189, 151, 98)',
						color: 'white',
						background: 'rgb(125, 125, 125)',
						backdrop: `url("${images[1]}") right bottom no-repeat`,
						showClass: {
							popup: 'animate__animated animate__fadeInUp animate__faster'
						},
						hideClass: {
							popup: 'animate__animated animate__fadeOutUp animate__faster'
						}
					})
				} else {
					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again.",
						confirmButtonColor: 'rgb(189, 151, 98)',
						color: 'white',
						background: 'rgb(125, 125, 125)',
						backdrop: `url("${images[0]}") right bottom no-repeat`,
						showClass: {
							popup: 'animate__animated animate__fadeInUp animate__faster'
						},
						hideClass: {
							popup: 'animate__animated animate__fadeOutUp animate__faster'
						}
					})
				}
			})
	}

	//USE EFFECT
	useEffect(() => {

	}, [])
	//user.email, user.gender, user.token, gender, modalShow

	return (
		(user.token == null) ?
			<Redirect to="/login" />
			:
			<Fragment>
				<AppNavbar />
				<Container fluid className="d-flex align-items-center p-0 m-0 illustration-box">
					<Row className="w-100 p-0 m-0 d-flex justify-content-center">
						<Col className="col-lg-10 col-sm-12">
							<h1 className="text-center">User Details</h1>
							<Form onSubmit={(e) => updateUser(e)}>
								<Table className="p-0 m-0 table-dark" striped bordered hover>
									<tbody>
										<tr>
											<th className="text-center">Email</th>
											<td className="text-center">
												<Form.Group className="mb-3">
													<Form.Control
														name="email"
														defaultValue={user.email}
														onChange={e => setEmail(e.target.value)}
														type="email"
														required
													/>
												</Form.Group>
											</td>
										</tr>
										<tr>
											<th className="text-center">Password</th>
											<td className="text-center">
												<Form.Group className="mb-3">
													<Form.Control
														name="password"
														type="password"
														placeholder="Set New Password (Min. 8 characters)"
														minLength="8"
														onChange={e => setNewPassword(e.target.value)}
													/>
												</Form.Group>
											</td>
										</tr>
										<tr>
											<th className="text-center">Nickname</th>
											<td className="text-center">
												<Form.Control
													type="text"
													defaultValue={user.nickname}
													onChange={e => setNickname(e.target.value)}
													minLength="4"
													maxLength="15"
													required
												/>
											</td>
										</tr>
										<tr>
											<th className="text-center">Gender</th>
											<td className="text-center">
												<Form.Group>
													<ButtonGroup className="my-2 w-100">
														<Row className="w-100">
															{radios.map((radio, idx) => (
																<Col className="col-4" key={idx}>
																	<ToggleButton
																		className="w-100"
																		id={`radio-${idx}`}
																		type="radio"
																		variant={idx % 2 ? 'outline-secondary' : 'outline-secondary'}
																		name="radio"
																		value={radio.value}
																		checked={gender === radio.value}
																		onChange={(e) => setGender(e.currentTarget.value)}
																		required
																	>
																		{radio.name}
																	</ToggleButton>
																</Col>
															))}
														</Row>
													</ButtonGroup>
												</Form.Group>
											</td>
										</tr>
										<tr>
											<th className="text-center">Avatar</th>
											<td className="text-center">
												{
													(isImageSelected === false) ?
														<Fragment>
															<Col className="col-12 mt-2 d-flex justify-content-center">
																<img
																	src={localStorage.getItem('profilePictureLocal')}
																	className="avatar-photo-preview rounded-circle" />
															</Col>
														</Fragment>
														:
														<Fragment>
															<Col className="col-12 mt-2 d-flex justify-content-center">
																<img
																	src={previewImage}
																	className="avatar-photo-preview rounded-circle" />
															</Col>
															<Col className="col-12 d-flex justify-content-center">
																<Button className="mt-2 ml-5"
																	variant="secondary"
																	onClick={(e) => resetAvatar(e)}
																>Reset</Button>
															</Col>
														</Fragment>
												}
												<Col className="col-12 d-flex justify-content-center">
													<Form.Control
														className="mt-2 w-25 btn text-white btn-secondary mr-2"
														id="avatar-photo"
														name="newImage"
														accept="image/png, image/jpeg"
														type="file"
														onClick={() => setModalShow(true)}
														onChange={(e) => handleNewImage(e)}
													/>
													<Button className="mt-2 ml-5"
														variant="danger"
														onClick={(e) => removeAvatar(e)}
													>Remove Avatar</Button>
												</Col>
											</td>
										</tr>
									</tbody>
									<tfoot>
										<tr>
											<td colSpan="2" className="">
												{
													(disableSubmit === true) ?
														<Col className="col-12 d-flex justify-content-end">
															<Button type="submit" className="btn btn-secondary" disabled>Save Changes</Button>
														</Col>
														:
														<Col className="col-12 d-flex justify-content-end">
															<Button type="submit" className="btn btn-secondary">Save Changes</Button>
														</Col>

												}
											</td>
										</tr>
									</tfoot>
								</Table>
							</Form>
						</Col>
					</Row>

				</Container>
				<Modal
					data-backdrop="static"
					data-keyboard="false"
					className="avatar-modal"
					size="lg"
					aria-labelledby="contained-modal-title-vcenter"
					centered
					show={modalShow}
					onHide={() => setModalShow(false)}
				>
					<Modal.Header>
						<Modal.Title id="contained-modal-title-vcenter">
							Please select your avatar picture
						</Modal.Title>
						<CloseButton variant="white" onClick={(e) => CancelChanges(e)} />
					</Modal.Header>
					<Modal.Body>
						<Row>
							<Col className="col-12 d-flex align-items-center justify-content-center">
								<AvatarEditor
									ref={(e) => setEditorRef(e)}
									scale={parseFloat(scale)}
									width={width}
									height={height}
									position={position}
									onPositionChange={(e) => handlePositionChange(e)}
									rotate={parseFloat(rotate)}
									borderRadius={200}
									image={image}
									className="editor-canvas"
								/>
							</Col>
							<Col className="col-12 d-flex align-items-center justify-content-center mt-4">
								<h4>Zoom</h4>
								<input
									name="scale"
									type="range"
									onChange={(e) => handleScale(e)}
									min={allowZoomOut ? '0.1' : '1'}
									max="2"
									step="0.01"
									defaultValue="1"
								/>
							</Col>
						</Row>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={(e) => CancelChanges(e)}>
							Cancel
						</Button>
						<Button variant="primary" onClick={(e) => SaveChanges(e)}>
							Save Changes
						</Button>
					</Modal.Footer>
				</Modal>
			</Fragment>
	)
}
