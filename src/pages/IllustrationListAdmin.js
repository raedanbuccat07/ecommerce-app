import React, { Fragment, useState, useContext, useEffect } from 'react';
import { Container, Row, Col, Modal, CloseButton, Card, Button, Image } from 'react-bootstrap';
import { Redirect } from 'react-router-dom'
import AppNavbar from '../components/AppNavbar'
import UserContext from '../UserContext'
import axios from 'axios'
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter'
import paginationFactory from 'react-bootstrap-table2-paginator';
import Swal from 'sweetalert2'
import { Scrollbars } from 'react-custom-scrollbars-2';
import Spinner from "react-spinners/ScaleLoader";

export default function IllustrationListAdmin() {

	let [loading, setLoading] = useState(true);
	let [loading2, setLoading2] = useState(false);
	let [color, setColor] = useState("#ffffff");
	//<Spinner color={color} loading={true} />
	const { user, setUser } = useContext(UserContext);
	//
	const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));
	function importAll(r) {
		return r.keys().map(r);
	}

	const [showIllustrationModal, setShowIllustrationModal] = useState(false);
	const [illustrationPreview, setIllustrationPreview] = useState([])
	function handleShow(data) {
		setIllustrationPreview(data)
		setShowIllustrationModal(true);
	}

	const [data, setData] = useState([])
	const [reload, setReload] = useState()

	const getData = () => {
		setLoading2(true)
		const config = {
			headers: { Authorization: `Bearer ${user.token}` }
		}
		axios("https://enigmatic-inlet-99767.herokuapp.com/api/products/admin", config).then((res) => {
			setData(res.data)
		})
		setLoading(false)
		setLoading2(false)
		return true
	}

	const headerFormat = (column, colIndex, { sortElement, filterElement }) => {
		return <Row>
			<Col className="col-12 my-2">
				<h5>{column.text} {sortElement}</h5>
			</Col>
			<Col className="col-12">
				{filterElement}
			</Col>
		</Row>
	}

	const previewFormatter = (data, row) => {
		return <span>
			<Row>
				<Col className="d-flex justify-content-center col-12">
					<Card className="card-product" style={{ width: '10rem', height: '10rem' }}>
						<Card.Img
							className="checkout-preview-image rounded"
							variant="top"
							src={data} />
					</Card>
				</Col>
			</Row>
		</span>
	}

	const setIsActiveFeatured = (isActive, data, mode) => {

		fetch(`https://enigmatic-inlet-99767.herokuapp.com/api/products/${data._id}/${mode}`, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${user.token}`
			}
		})
			.then(res => res.json())
			.then(async data => {
				if (data == true) {
					Swal.fire({
						position: 'top-end',
						icon: 'success',
						showConfirmButton: false,
						timer: 500,
						color: 'white',
						background: 'rgb(125, 125, 125)',
						backdrop: `url("${images[1]}") right bottom no-repeat`,
						showClass: {
							popup: 'animate__animated animate__fadeInUp animate__faster'
						},
						hideClass: {
							popup: 'animate__animated animate__fadeOutUp animate__faster'
						}
					})

					const waitMe = await getData()

					if (waitMe) {
						setReload()
					}
				}
			})
	}

	const actionFormatter = (data, row) => {
		return <span>
			<Row>
				<Col className="d-flex justify-content-center col-12 mt-2">
					{
						row.isActive === true ?
							<Button variant="danger" className="w-100" onClick={() => setIsActiveFeatured(row.isActive, row, "archive")}>Archive</Button>
							:
							<Button variant="success" className="w-100" onClick={() => setIsActiveFeatured(row.isActive, row, "unarchive")}>Unarchive</Button>
					}
				</Col>
				{
					row.isActive === true ?
						<Col className="d-flex justify-content-center col-12 mt-5">
							{
								row.isFeatured === true ?
									<Button variant="danger" className="w-100" onClick={() => setIsActiveFeatured(row.isFeatured, row, "unfeature")}>Remove Featured</Button>
									:
									<Button variant="success" className="w-100" onClick={() => setIsActiveFeatured(row.isFeatured, row, "feature")}>Set Featured</Button>
							}
						</Col>
						:
						<></>
				}
			</Row>
		</span>
	}

	const columns = [
		{
			dataField: 'productPicture',
			text: 'Preview',
			headerFormatter: (headerFormat),
			formatter: previewFormatter,
			headerStyle: (colum, colIndex) => {
				return {
					textAlign: 'center',
					color: 'white',
					backgroundColor: '#634a1b',
					cursor: 'pointer',
				};
			},
			style: {
				color: 'white',
				cursor: 'pointer'
			},
			events: {
				onClick: (e, column, columnIndex, row, rowIndex) => handleShow(row),
			},
		},
		{
			dataField: 'name',
			text: 'Title',
			headerFormatter: (headerFormat),
			headerStyle: (colum, colIndex) => {
				return {
					textAlign: 'center',
					color: 'white',
					backgroundColor: '#634a1b',
					cursor: 'pointer',
				};
			},
			style: {
				color: 'white',
				cursor: 'pointer'
			},
			filter: textFilter(),
			sort: true,
			events: {
				onClick: (e, column, columnIndex, row, rowIndex) => handleShow(row),
			},
		},
		{
			dataField: 'createdOn',
			text: 'Created On',
			headerFormatter: (headerFormat),
			headerStyle: (colum, colIndex) => {
				return {
					textAlign: 'center',
					color: 'white',
					backgroundColor: '#634a1b',
					cursor: 'pointer',
				};
			},
			style: {
				color: 'white',
				cursor: 'pointer'
			},
			filter: textFilter(),
			sort: true,
			events: {
				onClick: (e, column, columnIndex, row, rowIndex) => handleShow(row),
			},
		},
		{
			dataField: 'userData[0].nickname',
			text: 'Nickname',
			headerFormatter: (headerFormat),
			headerStyle: (colum, colIndex) => {
				return {
					textAlign: 'center',
					color: 'white',
					backgroundColor: '#634a1b',
					cursor: 'pointer',
				};
			},
			style: {
				color: 'white',
				cursor: 'pointer'
			},
			filter: textFilter(),
			sort: true,
			events: {
				onClick: (e, column, columnIndex, row, rowIndex) => handleShow(row),
			},
		},
		{
			dataField: '_id',
			text: 'ID',
			headerFormatter: (headerFormat),
			headerStyle: (colum, colIndex) => {
				return {
					textAlign: 'center',
					color: 'white',
					backgroundColor: '#634a1b',
					cursor: 'pointer',
				};
			},
			style: {
				color: 'white',
				cursor: 'pointer'
			},
			filter: textFilter(),
			sort: true,
			events: {
				onClick: (e, column, columnIndex, row, rowIndex) => handleShow(row),
			},
		},
		{
			dataField: 'isActive',
			text: 'Active',
			headerFormatter: (headerFormat),
			headerStyle: (colum, colIndex) => {
				return {
					textAlign: 'center',
					color: 'white',
					backgroundColor: '#634a1b',
					cursor: 'pointer',
				};
			},
			style: {
				color: 'white',
				cursor: 'pointer'
			},
			filter: textFilter(),
			sort: true,
			events: {
				onClick: (e, column, columnIndex, row, rowIndex) => handleShow(row),
			},

		},
		{
			dataField: 'isFeatured',
			text: 'Featured',
			headerFormatter: (headerFormat),
			headerStyle: (colum, colIndex) => {
				return {
					textAlign: 'center',
					color: 'white',
					backgroundColor: '#634a1b',
					cursor: 'pointer',
				};
			},
			style: {
				color: 'white',
				cursor: 'pointer'
			},
			filter: textFilter(),
			sort: true,
			events: {
				onClick: (e, column, columnIndex, row, rowIndex) => handleShow(row),
			},
		},
		{
			dataField: 'Action',
			text: 'Action',
			formatter: actionFormatter,
			headerFormatter: (headerFormat),
			headerStyle: (colum, colIndex) => {
				return {
					textAlign: 'center',
					color: 'white',
					backgroundColor: '#634a1b',
					cursor: 'pointer',
				};
			},
			style: {
				color: 'white',
				cursor: 'pointer'
			}
		},
	]

	const defaultSorted = [{
		dataField: 'createdOn',
		order: 'asc'
	}]

	const options = {
		paginationSize: 4,
		pageStartIndex: 0,
		firstPageText: 'First',
		prePageText: 'Back',
		nextPageText: 'Next',
		lastPageText: 'Last',
		nextPageTitle: 'First page',
		prePageTitle: 'Pre page',
		firstPageTitle: 'Next page',
		lastPageTitle: 'Last page',
		showTotal: true,
		disablePageTitle: true,
		sizePerPageList: [{
			text: '5', value: 5
		}, {
			text: '10', value: 10
		}, {
			text: '25', value: 25
		}, {
			text: '50', value: 50
		}, {
			text: 'All', value: data.length
		}]
	};


	useEffect(() => {
		getData()
	}, [reload])

	// || user.isAdmin !== true
	return (
		(user.token == null) ?
			<Redirect to="/login" />
			:
			<Fragment>
				<AppNavbar />
				{
					(loading === true) ?
						<Container fluid className="illustration-box d-flex justify-content-center align-items-center">
							<Container fluid className="p-0 m-0 d-flex justify-content-center align-items-center">
								<Row className="p-0 m-0 w-100 d-flex justify-content-center align-items-center">
									<Col className="universal-box col-lg-12 col-md-12 col-sm-12 p-0 mt-2 d-flex justify-content-center align-items-center">
										<Spinner color={color} loading={loading} />
									</Col>
								</Row>
							</Container>
						</Container>
						:
						<Fragment>
							<Container fluid className="illustration-box">
								<Container fluid className="p-0 m-0 d-flex justify-content-center">
									<Row className="p-0 m-0 w-100">
										<Col className="universal-box col-lg-12 col-md-12 col-sm-12 p-0 mt-2">
											<Row className="w-100 p-0 m-0 d-flex justify-content-center">
												<Fragment>
													<Col className="mt-3 col-12 p-0 m-0 d-flex justify-content-center">
													<Spinner color={color} loading={loading2} /><h1>Illustration List</h1><Spinner color={color} loading={loading2} />
													</Col>

													<Col className="mt-3 col-12 p-0 m-0">
														<BootstrapTable
															striped
															hover
															condensed
															bootstrap4
															keyField="_id"
															data={data}
															columns={columns}
															defaultSorted={defaultSorted}
															filter={filterFactory()}
															pagination={paginationFactory(options)}
														/>
													</Col>
												</Fragment>
											</Row>
										</Col>
									</Row>
								</Container>
							</Container>
							{
								(showIllustrationModal === true) ?
									<Modal
										show={showIllustrationModal}
										fullscreen={true}
										onHide={() => setShowIllustrationModal(false)}
										className="illustration-modal"
									>
										<Modal.Header className="illustration-modal">
											<Modal.Title className="text-center w-100">{illustrationPreview.name}</Modal.Title>

											<CloseButton variant="white" onClick={() => setShowIllustrationModal(false)} />
										</Modal.Header>
										<Modal.Body className="illustration-modal d-flex justify-content-center">
											<Container fluid className="d-flex justify-content-center">
												<Scrollbars
													renderThumbHorizontal={({ style, ...props }) =>
														<div {...props} style={{ ...style, width: '3px', borderRadius: '4px', boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.16)', backgroundColor: '#be913d' }} />
													}
													renderTrackVertical={({ style, ...props }) =>
														<div {...props} style={{ ...style, backgroundColor: 'black', right: '2px', bottom: '2px', top: '2px', borderRadius: '3px', width: '5px' }} />
													}
													renderThumbVertical={({ style, ...props }) =>
														<div {...props} style={{ ...style, width: '3px', borderRadius: '4px', boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.16)', backgroundColor: '#be913d' }} />
													}>
													<Container fluid className="d-flex justify-content-center">
														<Image className="product-picture-modal" src={illustrationPreview.productPicture} />
													</Container>

												</Scrollbars>
											</Container>
										</Modal.Body>
									</Modal>
									:
									<></>
							}
						</Fragment>
				}

			</Fragment>
	)
}
