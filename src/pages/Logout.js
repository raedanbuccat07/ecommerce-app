import { Redirect } from 'react-router-dom'
import { useContext, useEffect } from 'react';
import UserContext from '../UserContext'


export default function Login() {
    
    const {unsetUser, setUser} = useContext(UserContext)

    //clear local storage of the user's information
    //function from App.js
    unsetUser();

    useEffect(() => {
        // Set the user state back to it's original value

        setUser({
            id: null,
            email: null,
            nickname: null,
            gender: null,
            profilePicture: null,
            isAdmin: null,
            token: null  
        })
    },[])
    return(
        <Redirect to='/' />
    )
}