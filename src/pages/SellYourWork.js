import { Fragment, useState, useContext, useEffect } from 'react';
import { Container, Row, Col, Form, Button, Image, Card, Modal, CloseButton, ProgressBar } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import AppNavbar from '../components/AppNavbar'
import UserContext from '../UserContext';
import Swal from 'sweetalert2'
import axios from 'axios'
import Spinner from "react-spinners/PropagateLoader";

export default function SellYourWork() {

    const [percentage, setPercentage] = useState(0)
    const progressInstance = <ProgressBar now={percentage} label={`${percentage}%`} />;
    let [loading, setLoading] = useState(true);
    let [color, setColor] = useState("#ffffff");

    /* 
    
    <Spinner color={color} loading={loading} />
    */
    ////////////
    const { user, setUser } = useContext(UserContext);
    const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));
    function importAll(r) {
        return r.keys().map(r);
    }
    const [productName, setProductName] = useState('');
    const [productDescription, setProductDescription] = useState('');
    const [price, setPrice] = useState('');
    const [productPicturePreview, setProductPicturePreview] = useState();
    const [productPicture, setProductPicture] = useState();
    const [disableSubmit, setDisableSubmit] = useState(false)

    //MODAL
    const [showIllustrationModal, setShowIllustrationModal] = useState(false);
    function handleShow() {
        setShowIllustrationModal(true);
    }
    //

    const blobToBase64 = async (blob) => {
        return new Promise((resolve, _) => {
            const reader = new FileReader();
            reader.onloadend = () => resolve(reader.result);
            reader.readAsDataURL(blob);
        });
    }

    const submitFormData = async (e) => {
        e.preventDefault();

        const blobConvert = await blobToBase64(productPicture)

        const config = {
            headers: { Authorization: `Bearer ${user.token}` },
            onUploadProgress: (progressEvent) => {
                const { loaded, total } = progressEvent;
                let percent = Math.floor((loaded * 100) / total)
                setPercentage(percent)
            }
        };


        setDisableSubmit(true)
        axios.post('https://enigmatic-inlet-99767.herokuapp.com/api/products', {
            name: productName,
            description: productDescription,
            price: price,
            file: blobConvert
        }, config)
            .then((response) => {
                if (response.data === true) {
                    Swal.fire({
                        title: "Successfully posted",
                        icon: "success",
                        text: "You have successfully posted your work for sale.",
                        confirmButtonColor: 'rgb(189, 151, 98)',
                        color: 'white',
                        background: 'rgb(125, 125, 125)',
                        backdrop: `url("${images[1]}") right bottom no-repeat`,
                        showClass: {
                            popup: 'animate__animated animate__fadeInUp animate__faster'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutUp animate__faster'
                        }
                    })
                    setPercentage(0)
                    setDisableSubmit(false)
                    setProductName("")
                    setProductDescription("")
                    setPrice("")
                    setProductPicturePreview()
                    setProductPicture()
                    document.getElementById("product-photo").value = ""
                } else {
                    Swal.fire({
                        title: "Something went wrong",
                        icon: "error",
                        text: "Please try again.",
                        confirmButtonColor: 'rgb(189, 151, 98)',
                        color: 'white',
                        background: 'rgb(125, 125, 125)',
                        backdrop: `url("${images[0]}") right bottom no-repeat`,
                        showClass: {
                            popup: 'animate__animated animate__fadeInUp animate__faster'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutUp animate__faster'
                        }
                    })
                }
            })
    };

    const onChangePicture = (e) => {
        setProductPicture(e.target.files[0])
    }

    useEffect(() => {
        if (productPicture !== undefined) {
            setProductPicturePreview(URL.createObjectURL(productPicture))
        } else {
            setProductPicturePreview()
        }
    }, [productPicture])


    return (
        (user.token == null || user.isAdmin === true) ?
            <Redirect to="/login" />
            :
            <Fragment>
                <AppNavbar />
                <Container fluid className="illustration-box">
                    <Container fluid className="p-0 d-flex justify-content-center">
                        <Row className='w-100 p-0 my-5'>
                            <Col className="col-sm-12 col-lg-8">
                                <Card className="card-product overflow d-flex justify-content-center align-items-center" style={{ width: '100%' }}>
                                    <Card.Img
                                        onClick={() => handleShow()}
                                        className="card-image-style-view rounded illustration-view-hover"
                                        variant="top"
                                        src={productPicturePreview} />
                                </Card>
                            </Col>
                            <Col className="col-sm-12 col-lg-4">
                                <Row className="mt-5">
                                    <Form onSubmit={submitFormData}>
                                        <Col className='mt-5 col-12 d-flex justify-content-start align-items-center p-0 m-0'>
                                            <Row className="w-100 p-0 m-0">
                                                <Form.Group>
                                                    <Col className="col-12 d-flex justify-content-center">
                                                        <h5>Illustration (Required)</h5>
                                                    </Col>
                                                    <Col className="d-flex justify-content-center align-items-center">
                                                        <Form.Control
                                                            onChange={onChangePicture}
                                                            className="w-100 btn text-white btn-secondary"
                                                            id="product-photo"
                                                            name="newImage"
                                                            accept="image/png, image/jpeg"
                                                            type="file"
                                                            required
                                                        />
                                                    </Col>
                                                </Form.Group>
                                            </Row>
                                        </Col>
                                        <Col className='mt-3 col-12 d-flex justify-content-start align-items-center p-0 m-0'>
                                            <Row className="w-100 p-0 m-0">
                                                <Form.Group>
                                                    <Col className="col-12 d-flex justify-content-center">
                                                        <h5>Title*</h5>
                                                    </Col>
                                                    <Col className="d-flex justify-content-center align-items-center">
                                                        <Form.Control
                                                            name="productName"
                                                            type="text"
                                                            placeholder="Name"
                                                            value={productName}
                                                            onChange={(e) => setProductName(e.target.value)}
                                                            required
                                                        />
                                                    </Col>
                                                </Form.Group>

                                            </Row>
                                        </Col>
                                        <Col className='mt-3 col-12 d-flex justify-content-start align-items-center p-0 m-0'>
                                            <Row className="w-100 p-0 m-0">
                                                <Form.Group>
                                                    <Col className="col-12 d-flex justify-content-center">
                                                        <h5>Price*</h5>
                                                    </Col>
                                                    <Col className="d-flex justify-content-center align-items-center">
                                                        <Form.Group className="mb-3">
                                                            <Form.Control
                                                                name="productPrice"
                                                                type="number"
                                                                placeholder="Price"
                                                                value={price}
                                                                onChange={(e) => setPrice(e.target.value)}
                                                                required
                                                            />
                                                        </Form.Group>
                                                    </Col>
                                                </Form.Group>

                                            </Row>
                                        </Col>
                                        <Col className='mt-3 col-12 d-flex justify-content-start align-items-center p-0 m-0'>
                                            <Row className="w-100 p-0 m-0">
                                                <Form.Group>
                                                    <Col className="col-12 d-flex justify-content-center">
                                                        <h5>Description*</h5>
                                                    </Col>
                                                    <Col className="d-flex justify-content-center align-items-center">
                                                        <Form.Control
                                                            name="productDescription"
                                                            type="text"
                                                            as="textarea"
                                                            placeholder="Description"
                                                            value={productDescription}
                                                            onChange={(e) => setProductDescription(e.target.value)}
                                                            required
                                                        />
                                                    </Col>
                                                </Form.Group>

                                            </Row>
                                        </Col>
                                        <Col className='mt-5 col-12 d-flex justify-content-start align-items-center p-0 m-0'>
                                            <Row className="w-100 p-0 m-0">
                                                {
                                                    (disableSubmit === true) ?
                                                        <Fragment>
                                                            <Button variant="primary btn-login border-bottom w-100 h-100 rounded-0" type="submit" disabled>
                                                                Post
                                                            </Button>

                                                            <Row>
                                                                <Col className="col-12">
                                                                    {progressInstance}
                                                                </Col>
                                                                <Col className="col-12 d-flex justify-content-center">
                                                                    <Spinner color={color} loading={loading} />
                                                                </Col>
                                                            </Row>

                                                        </Fragment>
                                                        :
                                                        <Button variant="primary btn-login border-bottom w-100 h-100 rounded-0" type="submit">
                                                            Post
                                                        </Button>

                                                }
                                            </Row>
                                        </Col>
                                    </Form>
                                </Row>
                            </Col>
                        </Row>
                    </Container>
                </Container>
                <Modal
                    show={showIllustrationModal}
                    fullscreen={true}
                    onHide={() => setShowIllustrationModal(false)}
                    className="illustration-modal"
                >
                    <Modal.Header className="illustration-modal">
                        <CloseButton variant="white" onClick={() => setShowIllustrationModal(false)} />
                    </Modal.Header>
                    <Modal.Body className="illustration-modal d-flex justify-content-center">
                        <Image src={productPicturePreview} />

                    </Modal.Body>
                </Modal>
            </Fragment>
    )
}
