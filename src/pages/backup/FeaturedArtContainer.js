import { Container, Row, Col } from 'react-bootstrap'
import axios from 'axios'
import { useHistory } from 'react-router-dom'
import { useEffect, useState, Fragment } from 'react'
import { Carousel } from 'react-responsive-carousel';
import Spinner from "react-spinners/ScaleLoader";

export default function FeaturedArtContainer() {

    const history = useHistory()
    let [loading, setLoading] = useState(true);
    let [color, setColor] = useState("#ffffff");
    const [featuredData, setFeaturedData] = useState([])

    const getFeatured = async () => {
        setLoading(true)
        await axios.get(`https://enigmatic-inlet-99767.herokuapp.com/api/products/featured`)
            .then((result) => {
                setFeaturedData(result.data)
                setLoading(false)
            })
    }

    useEffect(() => {
        getFeatured()
    }, [])

    const viewIllustration = (illustrationId, sellerId) => {
        history.push(`/illustrations/view/${illustrationId}/${sellerId}`)
    }

    return (
        <Container className="p-0 m-0 featured-container home-outside-container">

            <Row className="w-100 p-0 m-0">
                <Col className="col-12 p-0 my-2">
                    <h1 className="text-center my-3">Featured Illustrations</h1>

                </Col>
                {
                    (loading === true) ?
                        <Col className="col-12 loader-col p-0 mt-5 d-flex justify-content-center align-items-center">
                            <Spinner color={color} loading={true} />
                        </Col>
                        :
                        <Col className="">
                            {
                                (featuredData.length !== 0) ?
                                    <Fragment>
                                        <Carousel
                                            infiniteLoop={true}
                                            showArrows={true}
                                            autoPlay={true}
                                            interval={2500}
                                            showThumbs={false}
                                            showStatus={false}
                                            stopOnHover={true}
                                            emulateTouch={true}
                                        >
                                            {
                                                featuredData.map((data, index) => {
                                                    return (
                                                        <div
                                                            key={index}
                                                            onClick={() => viewIllustration(data._id, data.sellerId)}
                                                            style={{ cursor: "pointer" }}
                                                        >
                                                            <img
                                                                style={{ cursor: "pointer" }}
                                                                className="carousel-image-entity"
                                                                src={data.productPicture}
                                                            />
                                                        </div>

                                                    )
                                                })
                                            }
                                        </Carousel>
                                    </Fragment>
                                    :
                                    <></>
                            }
                        </Col>
                }
            </Row>
        </Container>
    )
}