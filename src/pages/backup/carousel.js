import { Container, Row, Col, Image, Card, Button } from 'react-bootstrap'
import Carousel from 'react-multi-carousel'
import axios from 'axios'
import { useEffect, useState } from 'react'
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';

export default function FeaturedArtContainer() {

    /*     const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));
        function importAll(r) {
            return r.keys().map(r);
        } */

    /*     const responsive = {
            superLargeDesktop: {
                // the naming can be any, depends on you.
                breakpoint: { max: 4000, min: 3000 },
                items: 5
            },
            desktop: {
                breakpoint: { max: 3000, min: 1024 },
                items: 3
            },
            tablet: {
                breakpoint: { max: 1024, min: 464 },
                items: 2
            },
            mobile: {
                breakpoint: { max: 464, min: 0 },
                items: 1
            }
        };
    
     */
        .featured-container {
            background: rgba(24, 26, 27, 1);
          }
          
          .carousel-container {
            border: 1px red solid; 
            width: 100%;
            max-height: 90vh;
           }


    const responsive = {
        desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 1,
            paritialVisibilityGutter: 60
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 1,
            paritialVisibilityGutter: 50
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 1,
            paritialVisibilityGutter: 30
        }
    }

    const [featuredData, setFeaturedData] = useState([])

    const images = [
        "https://images.unsplash.com/photo-1549989476-69a92fa57c36?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
        "https://images.unsplash.com/photo-1549396535-c11d5c55b9df?ixlib=rb-1.2.1&auto=format&fit=crop&w=800&q=60",
        "https://images.unsplash.com/photo-1550133730-695473e544be?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
        "https://images.unsplash.com/photo-1550167164-1b67c2be3973?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
        "https://images.unsplash.com/photo-1550338861-b7cfeaf8ffd8?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
        "https://images.unsplash.com/photo-1550223640-23097fc71cb2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
        "https://images.unsplash.com/photo-1550353175-a3611868086b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
        "https://images.unsplash.com/photo-1550330039-a54e15ed9d33?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
        "https://images.unsplash.com/photo-1549737328-8b9f3252b927?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
        "https://images.unsplash.com/photo-1549833284-6a7df91c1f65?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
        "https://images.unsplash.com/photo-1549985908-597a09ef0a7c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60",
        "https://images.unsplash.com/photo-1550064824-8f993041ffd3?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=60"
    ]

    const getFeatured = async () => {
        await axios.get(`https://enigmatic-inlet-99767.herokuapp.com/api/products/featured`)
            .then((result) => {
                setFeaturedData(result.data)
            })

        /*         setMyIllustrations(
                    res.data.map((illustrations) => {
                        return (
                            <MyIllustrations key={illustrations._id} illustrationProp={illustrations} reloadList={reloadList} />
                        )
                    })
                ) */
    }

    useEffect(() => {
        getFeatured()
    }, [])

    /*   
      <Card style={{ width: '18rem' }}>
      <Card.Img variant="top" src="holder.js/100px180" />
      <Card.Body>
        <Card.Title>Card Title</Card.Title>
        <Card.Text>
          Some quick example text to build on the card title and make up the bulk of
          the card's content.
        </Card.Text>
        <Button variant="primary">Go somewhere</Button>
      </Card.Body>
    </Card>
    <Card style={{ width: '100%' }}>
                                    <Card.Img variant="top" src={data.productPicture} />
                                </Card>
                                    <Image
                                        fluid
                                        draggable={false}
                                        style={{ width: "100%", height: "100%" }}
                                        src={data.productPicture}
                                    />
    
     */

    return (
        <Container className="p-0 m-0 featured-container home-outside-container">

            <Row className="">
                <Col className="col-12">
                    <h1 className="text-center my-3">Featured Illustrations</h1>
                </Col>
                <Col className="col-12">
                    <Carousel
                        centerMode={true}
                        containerClass="carousel-container"
                        itemClass="image-item"
                        responsive={responsive}
                        autoPlay={true}
                        infinite={true}
                        autoPlaySpeed={10000}
                    >
                        {featuredData.map(data => {
                            return (
                                <Image
                                    fluid
                                    draggable={false}
                                    style={{ width: "100%", height: "100%" }}
                                    src={data.productPicture}
                                />
                            )
                        })}
                    </Carousel>
                </Col>
            </Row>

        </Container>
    )
}