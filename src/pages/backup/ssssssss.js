myIllustrations.map((illustration, i) => {
    return (
        <Fragment>
            <Col key={illustration.price + i} className="my-3 mx-2 col-lg-2 col-sm-12 p-0 m-0 d-flex justify-content-center">
                <Card key={illustration._id + i} className="card-product" style={{ width: '20rem', height: '30rem' }}>
                    <Card.Img
                        key={illustration.productPicture + i}
                        className="card-image-style rounded"
                        variant="top"
                        src={`${process.env.PUBLIC_URL}/images/productPictures/${illustration.productPicture}`} />
                    <Card.Body key={illustration.sellerId + i} className="p-0 m-0 d-flex justify-content-center">
                        <Row className="w-100 p-0 m-0">
                            <Col className="col-12 d-flex justify-content-center p-0 m-0">
                                <Link
                                    key={illustration.name + i}
                                    className="h4 h-50 w-100 card-title btn btn-secondary mt-2 text-white text-center"
                                    to={`illustrations/view/${illustration._id}/${illustration.sellerId}`}>{illustration.name}
                                </Link>
                            </Col>
                        </Row>

                    </Card.Body>
                </Card>
            </Col>
        </Fragment>
    );
})


<Col className="col-12">
<ReactPaginate
previousLabel={'<'}
nextLabel={'>'}
breakLabel={'...'}
pageCount={pageCount2}
marginePagesDisplayed={3}
pageRangeDisplayed={3}
onPageChange={handlePageClick2}
containerClassName={'pagination justify-content-center'}
pageClassName={'page-item'}
pageLinkClassName={'page-link'}
previousClassName={'page-item'}
previousLinkClassName={'page-link'}
nextClassName={'page-item'}
nextLinkClassName={'page-link'}
breakClassName={'page-item'}
breakLinkClassName={'page-link'}
activeClassName={'active'}
/>
</Col>