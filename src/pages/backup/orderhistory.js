import { Fragment, useContext, useState, useEffect } from 'react';
import { Container, Row, Col, Table, Card } from 'react-bootstrap';
import AppNavbar from '../components/AppNavbar'
import UserContext from '../UserContext'
import axios from 'axios'
import { Redirect } from 'react-router-dom';

export default function OrderHistory() {
	const { user, setUser } = useContext(UserContext);
	const [orderHistory, setOrderHistory] = useState([])
	const [productsList, setProductsList] = useState([])

	useEffect(() => {

		const config = {
			headers: { Authorization: `Bearer ${user.token}` }
		};

		const getOrderHistory = async () => {
			try {
				const res = await axios.get(`https://enigmatic-inlet-99767.herokuapp.com/api/order/users/myOrders`, config);

				setProductsList(res.data)
				setOrderHistory(res.data)

			} catch (error) {
				console.error(error);
			}
		};
		getOrderHistory();


	}, [])

	return (
		(user.token == null || user.isAdmin === true) ?
			<Redirect to="/login" />
			:
			<Fragment>
				<AppNavbar />
				<Container fluid className="illustration-box">
					<Container fluid className="p-0 m-0 d-flex justify-content-center">
						<Row className="d-flex justify-content-center w-75">
							<Col className="col-12">
								<h1 className="text-center mt-5">Order History</h1>
							</Col>
							<Col className="col-12">
								{
									orderHistory.map((data, i) => {

										return (
											<Fragment key={data._id}>
												<Table className="p-0 mt-5 table-dark" striped bordered hover>
													<thead>
														<tr>
															<th colSpan="5">Ordered at {data.purchasedOn}</th>
														</tr>
														<tr>
															<th className="text-center">Preview Image</th>
															<th className="text-center">Name</th>
															<th className="text-center">Price</th>
															<th className="text-center">Quantity</th>
															<th className="text-center">Subtotal</th>
														</tr>
													</thead>
													<tbody>
														{
															productsList[i].productsPurchased.map((products, x) => {
																let subtotal = products.price * products.quantity
																return (
																	<tr key={products._id} id={`table-row-item`}>
																		<td className="d-flex justify-content-center">
																			<Card className="card-product" style={{ width: '10rem', height: '10rem' }}>
																				<Card.Img
																					className="checkout-preview-image rounded"
																					variant="top"
																					src={products.productPicture} />
																			</Card>
																		</td>
																		<td className="text-center">{products.productName}</td>
																		<td className="text-center">{products.price}</td>
																		<td className="text-center">{products.quantity}</td>
																		<td className="text-center">{subtotal}</td>
																	</tr>
																)
															})

														}

													</tbody>
													<tfoot>
														<tr>
															<td colSpan="5" id="total-number-text"><h4 className="text-end">Total: {data.totalAmount}</h4></td>
														</tr>
													</tfoot>
												</Table>
											</Fragment>

										)
									})
								}

							</Col>
						</Row>
					</Container>
				</Container>
			</Fragment >
	)
}
