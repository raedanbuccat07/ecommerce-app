import { Fragment, useState, useContext } from 'react';
import { Container, Row, Col, Navbar, Form, Button } from 'react-bootstrap';
import { NavLink, Link, Redirect } from 'react-router-dom';
import AppNavbarLogoOnly from '../components/AppNavbarLogoOnly'
import Swal from 'sweetalert2'
import UserContext from '../UserContext';

export default function LoginAccount() {

	function importAll(r) {
		return r.keys().map(r);
	}

	const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));
	const { user, setUser } = useContext(UserContext);

	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	async function authenticate(e) {
		e.preventDefault();

		await fetch('https://enigmatic-inlet-99767.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
			.then(res => res.json())
			.then(data => {
				if (typeof data.accessToken !== "undefined") {
					retrieveUserDetails(data.accessToken)
					setEmail('');
					setPassword('');
				} else {
					Swal.fire({
						title: "Error when logging in.",
						icon: "error",
						text: "Password or Email is wrong.",
						confirmButtonColor: 'rgb(189, 151, 98)',
						color: 'white',
						background: 'rgb(125, 125, 125)',
						backdrop: `url("${images[0]}") right bottom no-repeat`,
						showClass: {
							popup: 'animate__animated animate__fadeInUp animate__faster'
						},
						hideClass: {
							popup: 'animate__animated animate__fadeOutUp animate__faster'
						}
					})
				}
			})
	}

	const retrieveUserDetails = async (token) => {
		await fetch('https://enigmatic-inlet-99767.herokuapp.com/api/users/user/details', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
			.then(res => res.json())
			.then(data => {
				localStorage.setItem("profilePictureLocal", data.profilePicture)
				localStorage.setItem('token', token)
				localStorage.setItem("accountId", data._id)
				localStorage.setItem("gender", data.gender)
				localStorage.setItem("profilePictureIdLocal", data.profilePictureId)
				return data
			}).then(data => {
				setUser({
					id: data._id,
					email: data.email,
					nickname: data.nickname,
					gender: data.gender,
					profilePicture: data.profilePicture,
					isAdmin: data.isAdmin,
					token: token,
					profilePictureId: data.profilePictureId
				})
			})
	}

	return (
		(user.token !== null) ?
			<Redirect to="/" />
			:
			<Fragment>
				<AppNavbarLogoOnly />
				<Container className="login-container animate__fadeIn">
					<Row className="h-100 d-flex align-items-center justify-content-center p-0 m-0">
						<Col className="login-box col-md-4 col-sm-12 h-75 d-flex p-0 m-0">
							<Row className="w-100 p-0 m-0">
								<Col className="col-12 d-flex justify-content-center align-items-center">

									<Navbar.Brand className="text-white login-box-logo d-flex justify-content-center align-items-center" as={NavLink} to="/" exact>EMINA<small>MISE</small></Navbar.Brand>

								</Col>

								<Col className="col-12 d-flex justify-content-center">
									<Row className="w-100 h-100 p-0 m-0">
										<Col className="col-12">
											<Form className="mt-3" onSubmit={(e) => authenticate(e)}>
												<Form.Group className="mb-3" controlId="userEmail">
													<Form.Control
														type="email"
														placeholder="Email Address"
														value={email}
														onChange={(e) => setEmail(e.target.value)}
														required
													/>
													<Form.Text className="text-muted">
													</Form.Text>
												</Form.Group>

												<Form.Group className="mb-3" controlId="password">
													<Form.Control
														type="password"
														placeholder="Password"
														value={password}
														onChange={(e) => setPassword(e.target.value)}
														required
													/>
												</Form.Group>

												<div style={{ display: "flex", justifyContent: "space-around" }}>

													<Button variant="btn btn-login border-bottom w-100 h-50 rounded-0" type="submit">
														Login
													</Button>
												</div>

											</Form>
										</Col>

										<Col className="col-12 d-flex justify-content-center">
											<Row className="w-100 h-100 p-0 m-0">
												<Col className="col-12">
													<Link className="text-center text-white" as={NavLink} to="/login">Forgot Password?</Link>
												</Col>
											</Row>
										</Col>
										<Link className="text-center text-white" as={NavLink} to="/login">Go back</Link>

									</Row>


								</Col>
								<Col className="col-12 d-flex justify-content-center align-items-center border-top">
									<p className="text-white disclaimer-text">
										Ecommerce test project made with REACT JS
									</p>
								</Col>
							</Row>
						</Col>
					</Row>
				</Container>
			</Fragment>
	)
}
