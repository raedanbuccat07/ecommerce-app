import React, { Fragment, useState, useContext, useEffect } from 'react';
import { Redirect } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';
import AppNavbar from '../components/AppNavbar'
import MyIllustrations from '../components/MyIllustrations'
import UserContext from '../UserContext'
import axios from 'axios'
import ReactPaginate from 'react-paginate'
import Spinner from "react-spinners/ScaleLoader";

export default function ViewMyProfile() {

	let [loadingIllustration, setLoadingIllustration] = useState(true);
	let [loadingViewIllustration, setLoadingViewIllustration] = useState(false);
	let [firstTimeLoad, setFirstTimeLoad] = useState(true)
	let [color, setColor] = useState("#ffffff");
	//<Spinner color={color} loading={true} />
	const { user, setUser } = useContext(UserContext);
	//
	const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));
	function importAll(r) {
		return r.keys().map(r);
	}
	//ILLUST
	const [myIllustrations, setMyIllustrations] = useState([])
	const [pageCount2, setpageCount2] = useState(localStorage.getItem("viewMyIllustrationsPage"))
	let limitPage = 10
	const config = {
		headers: { Authorization: `Bearer ${user.token}` }
	};

	//

	const reloadList = () => {
		getMyIllustrations()
	}

	const resetPage = async (mode) => {
		switch (mode) {
			case "VIEW":
				localStorage.setItem("viewIllustrationsPage", 1)
				getMyIllustrations()
				break
		}
	}

	const getMyIllustrations = async () => {
		if (user.token !== null) {
			setLoadingIllustration(true)
			if (localStorage.getItem("viewMyIllustrationsPage") === null) {
				localStorage.setItem("viewMyIllustrationsPage", 1)
			}

			try {
				const res = await axios.get(`https://enigmatic-inlet-99767.herokuapp.com/api/products/seller/${localStorage.getItem("viewMyIllustrationsPage")}/MyProducts`, config);
				const totalRes = res.data.pop()
				const total = totalRes.totalAllResult;
				if (localStorage.getItem("viewIllustrationsPage") > (Math.ceil(total / limitPage))) {
					resetPage("VIEW")
				}
				setpageCount2(Math.ceil(total / limitPage));
				setMyIllustrations(
					res.data.map((illustrations, index) => {
						return (
							<MyIllustrations index={index} key={illustrations._id} illustrationProp={illustrations} reloadList={reloadList} />
						)
					})
				)
				setLoadingIllustration(false)
				setFirstTimeLoad(false)
			} catch (error) {

			}
		}
	}

	const fetchMyIllustrations = async (currentPage) => {
		if (user.token !== null) {
			setLoadingViewIllustration(true)
			try {
				const res = await axios.get(
					`https://enigmatic-inlet-99767.herokuapp.com/api/products/seller/${currentPage}/MyProducts`, config
				);
				const data = res.data;
				setLoadingViewIllustration(false)
				return data;
			} catch (error) {

			}
		}
	}

	const handlePageClick2 = async (data) => {
		setLoadingViewIllustration(true)
		let currentPage = data.selected + 1
		let illustrationsFormServer = ""
		localStorage.setItem("viewMyIllustrationsPage", currentPage)

		illustrationsFormServer = await fetchMyIllustrations(currentPage)
		illustrationsFormServer.pop()
		setMyIllustrations(
			illustrationsFormServer.map((illustrations, index) => {
				return (
					<MyIllustrations index={index} key={illustrations._id} illustrationProp={illustrations} />
				)
			})
		)
		setLoadingViewIllustration(false)
	}

	useEffect(() => {
		getMyIllustrations()
	}, [])

	return (
		(user.token == null || user.isAdmin === true) ?
			<Redirect to="/login" />
			:
			<Fragment>
				<AppNavbar />
				{
					(loadingIllustration === true && firstTimeLoad === true) ?
						<Container fluid className="illustration-box d-flex justify-content-center align-items-center">
							<Spinner color={color} loading={loadingIllustration} />
						</Container>
						:
						<Fragment>
							{
								(myIllustrations.length !== 0) ?
									<Container fluid className="illustration-box">
										<Container fluid className="p-0 m-0 d-flex justify-content-center">
											<Row className="p-0 m-0 w-100">
												<Col className="universal-box col-lg-12 col-md-12 col-sm-12 p-0 mt-2">
													<Row className="w-100 p-0 m-0 d-flex justify-content-center">
														<Fragment>
															<Col className="mt-3 col-12 p-0 m-0 d-flex justify-content-center">
																<Spinner color={color} loading={loadingViewIllustration} />
																<h1>My Illustrations</h1>
																<Spinner color={color} loading={loadingViewIllustration} />
															</Col>
															{myIllustrations}
															<Col className="col-12">
																<ReactPaginate
																	previousLabel={'<'}
																	nextLabel={'>'}
																	breakLabel={'...'}
																	pageCount={pageCount2}
																	marginePagesDisplayed={3}
																	pageRangeDisplayed={3}
																	forcePage={(localStorage.getItem("viewMyIllustrationsPage") - 1)}
																	onPageChange={handlePageClick2}
																	containerClassName={'pagination justify-content-center'}
																	pageClassName={'page-item'}
																	pageLinkClassName={'page-link'}
																	previousClassName={'page-item'}
																	previousLinkClassName={'page-link'}
																	nextClassName={'page-item'}
																	nextLinkClassName={'page-link'}
																	breakClassName={'page-item'}
																	breakLinkClassName={'page-link'}
																	activeClassName={'active'}
																/>
															</Col>
														</Fragment>
													</Row>
												</Col>
											</Row>
										</Container>
									</Container>
									:
									<Container fluid className="illustration-box d-flex justify-content-center align-items-center">
										<h1>You have no Illustrations yet</h1>
									</Container>
							}
						</Fragment>
				}

			</Fragment>
	)
}
