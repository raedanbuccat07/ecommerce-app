import { Fragment, useEffect, useContext, useState } from 'react';
import { Container, Row, Col, Modal, Button, Form, Card, Image, CloseButton, ProgressBar } from 'react-bootstrap'
import AppNavbar from '../components/AppNavbar'
import { useParams, Link, useHistory, useLocation } from 'react-router-dom'
import Avatar from 'react-avatar'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import axios from 'axios'
import Heart from "react-heart"
import SellerIllustrations2 from '../components/MyIllustrations2'
import { Scrollbars } from 'react-custom-scrollbars-2'
import ReactPaginate from 'react-paginate'
import Spinner from "react-spinners/ScaleLoader"
import Spinner2 from "react-spinners/PropagateLoader"
import { Fade } from "react-awesome-reveal"

export default function IllustrationsView() {

    let [loadingIllustration, setLoadingIllustration] = useState(true);
    let [loadingViewIllustration, setLoadingViewIllustration] = useState(true)
    let [firstTimeLoad, setFirstTimeLoad] = useState(true)
    let [color, setColor] = useState("#ffffff");
    //<Spinner color={color} loading={true} />
    const [percentage, setPercentage] = useState(0)
    const progressInstance = <ProgressBar now={percentage} label={`${percentage}%`} />
    let [loading, setLoading] = useState(true);
    const [disableSubmit, setDisableSubmit] = useState(false)
    const [reload, setReload] = useState()
    const history = useHistory()
    const { user, setUser } = useContext(UserContext);
    const { illustrationId, sellersId } = useParams()
    const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/))
    function importAll(r) {
        return r.keys().map(r);
    }

    //MODAL
    const [showIllustrationModal, setShowIllustrationModal] = useState(false);
    function handleShow() {
        setShowIllustrationModal(true);
    }

    //
    const [activeFavorites, setActiveFavorites] = useState(false)
    const [favoriteId, setFavoriteId] = useState()
    //
    const [productId, setProductId] = useState()
    const [name, setName] = useState()
    const [description, setDescription] = useState()
    const [price, setPrice] = useState()
    const [isActive, setIsActive] = useState()
    const [isFeatured, setIsFeatured] = useState()
    const [productPicture, setProductPicture] = useState()
    const [productPictureId, setProductPictureId] = useState()
    const [productPictureFetch, setProductPictureFetch] = useState()
    const [likes, setLikes] = useState()
    const [createdOn, setCreatedOn] = useState()
    const [favorites, setFavorites] = useState()
    //
    const [sellerId, setSellerId] = useState(0)
    const [nickname, setNickname] = useState()
    const [profilePicture, setProfilePicture] = useState()
    const [quantity, setQuantity] = useState(1)
    //


    //
    const [productNameUpdate, setProductNameUpdate] = useState()
    const [productPriceUpdate, setProductPriceUpdate] = useState()
    const [productDescriptionUpdate, setProductDescriptionUpdate] = useState()
    const [productPictureUpdate, setProductPictureUpdate] = useState()
    const [productPicturePreview, setProductPicturePreview] = useState()
    const onChangePicture = (e) => {
        setProductPicturePreview(URL.createObjectURL(e.target.files[0]))
        setProductPictureUpdate(e.target.files[0])
    }

    //
    const [sellerIllustrations, setSellerIllustrations] = useState([])
    const [pageCount, setpageCount] = useState(localStorage.getItem("sellerIllustrationsPage"))
    let limitPage = 10
    const config = {
        headers: { Authorization: `Bearer ${user.token}` },
        onUploadProgress: (progressEvent) => {
            const { loaded, total } = progressEvent;
            let percent = Math.floor((loaded * 100) / total)
            setPercentage(percent)
        }
    };
    //

    function setAsFavorite() {

        if (activeFavorites === false) {
            fetch(`https://enigmatic-inlet-99767.herokuapp.com/api/products/favorite`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${user.token}`
                },
                body: JSON.stringify({
                    productId: productId
                })
            })
            setActiveFavorites(true)
        } else if (activeFavorites === true) {
            fetch(`https://enigmatic-inlet-99767.herokuapp.com/api/products/unfavorite`, {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                    Authorization: `Bearer ${user.token}`
                },
                body: JSON.stringify({
                    productId: productId,
                    userId: user.id
                })
            })
            setActiveFavorites(false)
        }
    }

    function addToCart(e) {
        e.preventDefault()
        if (user.token !== null) {
            Swal.fire({
                title: "Add to Cart",
                icon: "question",
                text: "Add this illustration to your cart?",
                color: 'white',
                background: 'rgb(125, 125, 125)',
                backdrop: `url("${images[2]}") right bottom no-repeat`,
                showClass: {
                    popup: 'animate__animated animate__fadeInUp animate__faster'
                },
                hideClass: {
                    popup: 'animate__animated animate__fadeOutUp animate__faster'
                },
                showCancelButton: true,
                confirmButtonColor: 'rgb(189, 151, 98)',
                cancelButtonColor: 'rgb(115, 115, 115)',
                confirmButtonText: 'Confirm'
            }).then((result) => {
                if (result.isConfirmed) {
                    fetch('https://enigmatic-inlet-99767.herokuapp.com/api/cart/addCart', {
                        method: "POST",
                        headers: {
                            "Content-Type": "application/json",
                            Authorization: `Bearer ${user.token}`
                        },
                        body: JSON.stringify({
                            productId: productId,
                            quantity: quantity,
                            price: price,
                            sellerId: sellerId,
                            productPicture: productPictureFetch,
                            productName: name
                        })
                    })
                        .then(res => res.json())
                        .then(data => {
                            if (data === true) {
                                Swal.fire({
                                    icon: 'success',
                                    timer: 1000,
                                    color: 'white',
                                    background: 'rgb(125, 125, 125)',
                                    backdrop: `url("${images[1]}") right bottom no-repeat`,
                                    showClass: {
                                        popup: 'animate__animated animate__fadeInUp animate__faster'
                                    },
                                    hideClass: {
                                        popup: 'animate__animated animate__fadeOutUp animate__faster'
                                    }
                                })
                                setQuantity(1)
                            } else {
                                Swal.fire({
                                    title: "Something went wrong",
                                    icon: "error",
                                    text: "Please try again.",
                                    confirmButtonColor: 'rgb(189, 151, 98)',
                                    color: 'white',
                                    background: 'rgb(125, 125, 125)',
                                    backdrop: `url("${images[0]}") right bottom no-repeat`,
                                    showClass: {
                                        popup: 'animate__animated animate__fadeInUp animate__faster'
                                    },
                                    hideClass: {
                                        popup: 'animate__animated animate__fadeOutUp animate__faster'
                                    }
                                })
                            }
                        })
                }
            })
        } else {
            history.push("/login")
        }
    }

    function unarchiveProduct(e) {
        e.preventDefault()
        fetch(`https://enigmatic-inlet-99767.herokuapp.com/api/products/${productId}/unarchive`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${user.token}`
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data == true) {
                    setIsActive(true)
                    Swal.fire({
                        icon: 'success',
                        timer: 1000,
                        color: 'white',
                        background: 'rgb(125, 125, 125)',
                        backdrop: `url("${images[1]}") right bottom no-repeat`,
                        showClass: {
                            popup: 'animate__animated animate__fadeInUp animate__faster'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutUp animate__faster'
                        }
                    })
                    getSellerIllustrations()
                }
            })
    }

    function archiveProduct(e) {
        e.preventDefault()
        fetch(`https://enigmatic-inlet-99767.herokuapp.com/api/products/${productId}/archive`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${user.token}`
            }
        })
            .then(res => res.json())
            .then(data => {
                if (data == true) {
                    setIsActive(false)
                    Swal.fire({
                        icon: 'success',
                        timer: 1000,
                        color: 'white',
                        background: 'rgb(125, 125, 125)',
                        backdrop: `url("${images[1]}") right bottom no-repeat`,
                        showClass: {
                            popup: 'animate__animated animate__fadeInUp animate__faster'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutUp animate__faster'
                        }
                    })
                    getSellerIllustrations()
                }
            })
    }

    function unsetImageProduct(e) {
        e.preventDefault()
        document.getElementById("illustration-canvas-edit").value = null
    }

    function resetPriceProduct(e) {
        e.preventDefault()
        document.getElementById("price").value = price
    }

    function resetTitleProduct(e) {
        e.preventDefault()
        document.getElementById("title").value = name
    }

    function resetDescriptionProduct(e) {
        e.preventDefault()
        document.getElementById("description").value = description
    }

    const blobToBase64 = async (blob) => {
        return new Promise((resolve, _) => {
            const reader = new FileReader();
            reader.onloadend = () => resolve(reader.result);
            reader.readAsDataURL(blob);
        })
    }


    async function updateProduct(e) {
        e.preventDefault()
        let blobConvert = ""

        if (productPictureUpdate !== undefined && productPictureUpdate !== null) {
            blobConvert = await blobToBase64(productPictureUpdate)
            updateProduct2(blobConvert)
        } else {
            updateProduct2(blobConvert)
        }
    }

    const updateProduct2 = async (blobConvert) => {
        setDisableSubmit(true)
        axios.put(`https://enigmatic-inlet-99767.herokuapp.com/api/products/${productId}`, {
            name: productNameUpdate,
            price: productPriceUpdate,
            description: productDescriptionUpdate,
            file: blobConvert,
            productPictureId: productPictureId
        }, config)
            .then((response) => {
                if (response.data === true) {
                    setDisableSubmit(false)
                    setFirstTimeLoad(false)
                    setPercentage(0)
                    Swal.fire({
                        title: "Update",
                        icon: "success",
                        text: "You have successfully updated your canvas.",
                        confirmButtonColor: 'rgb(189, 151, 98)',
                        color: 'white',
                        background: 'rgb(125, 125, 125)',
                        backdrop: `url("${images[1]}") right bottom no-repeat`,
                        showClass: {
                            popup: 'animate__animated animate__fadeInUp animate__faster'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutUp animate__faster'
                        }
                    })
                    getIllustrationDetails()
                    getSellerIllustrations()
                } else {
                    setPercentage(0)
                    setDisableSubmit(false)
                    setFirstTimeLoad(false)
                    Swal.fire({
                        title: "Something went wrong",
                        icon: "error",
                        text: "Please try again.",
                        confirmButtonColor: 'rgb(189, 151, 98)',
                        color: 'white',
                        background: 'rgb(125, 125, 125)',
                        backdrop: `url("${images[0]}") right bottom no-repeat`,
                        showClass: {
                            popup: 'animate__animated animate__fadeInUp animate__faster'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutUp animate__faster'
                        }
                    })
                }
            })
    }

    const getIllustrationDetails = async () => {
        setLoadingViewIllustration(true)
        const res = await axios.get(`https://enigmatic-inlet-99767.herokuapp.com/api/products/${illustrationId}`);
        if (res.data === "") {
            history.push("/illustrations")
        } else if (res.data !== false) {
            let data = await res.data
            setName(data.name)
            setProductId(data._id)
            setSellerId(data.sellerId)
            setDescription(data.description)
            setPrice(data.price)
            setIsActive(data.isActive)
            setIsFeatured(data.isFeatured)
            setProductPictureFetch(data.productPicture)
            setProductPicture(data.productPicture)
            setProductPictureId(data.productPictureId)
            setLikes(data.likes)
            setCreatedOn(data.createdOn)
            setFavorites(data.favorites)

            if (data.name !== name) {
                try {
                    document.getElementById("price").value = data.price
                    document.getElementById("title").value = data.name
                    document.getElementById("description").value = data.description
                } catch (err) {

                }
            }

            if (data.favorites.length !== 0) {
                data.favorites.map((data, index) => {
                    if (data.userId === localStorage.getItem('accountId')) {
                        setActiveFavorites(true)
                        setFavoriteId(data._id)
                    }
                })
            } else {
                setActiveFavorites(false)
            }

            if (data != null) {
                await fetch(`https://enigmatic-inlet-99767.herokuapp.com/api/users/details/${data.sellerId}`)
                    .then(res => res.json())
                    .then(data => {
                        setNickname(data.nickname)
                        setProfilePicture(data.profilePicture)
                    }).then(() => {
                        return true
                    }).then(() => {
                        setLoadingViewIllustration(false)
                    })

            }
        }
    }

    const resetPage = async (mode) => {
        switch (mode) {
            case "SELLER":
                localStorage.setItem("sellerIllustrationsPage", 1)
                getSellerIllustrations()
                break
        }
    }

    const getSellerIllustrations = async () => {

        setLoadingIllustration(true)
        if (localStorage.getItem("sellerIllustrationsPage") === null) {
            localStorage.setItem("sellerIllustrationsPage", 1)
        }
        const res = await axios.get(`https://enigmatic-inlet-99767.herokuapp.com/api/products/page/seller/${sellersId}/${localStorage.getItem("sellerIllustrationsPage")}`, config);
        const totalRes = res.data.pop()
        const total = totalRes.totalAllResult;

        if (localStorage.getItem("sellerIllustrationsPage") > (Math.ceil(total / limitPage))) {
            resetPage("SELLER")
        }

        setpageCount(Math.ceil(total / limitPage));
        setSellerIllustrations(
            res.data.map((illustrations, index) => {
                return (
                    <SellerIllustrations2 index={index} key={illustrations._id} illustrationProp={illustrations} />
                )
            })
        )
        setLoadingIllustration(false)
    }

    const fetchSellerIllustrations = async (currentPage) => {
        const res = await axios.get(
            `https://enigmatic-inlet-99767.herokuapp.com/api/products/page/seller/${sellersId}/${currentPage}`, config
        );
        const data = res.data;

        return data;
    }

    const handlePageClick = async (data) => {
        setLoadingIllustration(true)
        let currentPage = data.selected + 1
        let illustrationsFormServer = ""
        localStorage.setItem("sellerIllustrationsPage", currentPage)

        illustrationsFormServer = await fetchSellerIllustrations(currentPage)
        illustrationsFormServer.pop()
        setSellerIllustrations(
            illustrationsFormServer.map((illustrations, index) => {
                return (
                    <SellerIllustrations2 index={index} key={illustrations._id} illustrationProp={illustrations} />
                )
            })
        )
        setLoadingIllustration(false)
    }

    const setIsActiveFeatured = (data, mode) => {

        fetch(`https://enigmatic-inlet-99767.herokuapp.com/api/products/${data}/${mode}`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${user.token}`
            }
        })
            .then(res => res.json())
            .then(async data => {
                if (data == true) {
                    Swal.fire({
                        position: 'top-end',
                        icon: 'success',
                        showConfirmButton: false,
                        timer: 500,
                        color: 'white',
                        background: 'rgb(125, 125, 125)',
                        backdrop: `url("${images[1]}") right bottom no-repeat`,
                        showClass: {
                            popup: 'animate__animated animate__fadeInUp animate__faster'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutUp animate__faster'
                        }
                    })
                    const waitMe = await getIllustrationDetails()
                    if (waitMe) {
                        setReload()
                    }
                }
            })
    }

    function deleteProduct(e, _id, productName) {
        e.preventDefault()

        Swal.fire({
            title: `Delete ${productName}`,
            icon: "question",
            text: "Are you sure you want to delete this Illustration?",
            color: 'white',
            background: 'rgb(125, 125, 125)',
            backdrop: `url("${images[2]}") right bottom no-repeat`,
            showClass: {
                popup: 'animate__animated animate__fadeInUp animate__faster'
            },
            hideClass: {
                popup: 'animate__animated animate__fadeOutUp animate__faster'
            },
            showCancelButton: true,
            confirmButtonColor: 'rgb(189, 151, 98)',
            cancelButtonColor: 'rgb(115, 115, 115)',
            confirmButtonText: 'Confirm'
        }).then((result) => {
            if (result.isConfirmed) {
                fetch(`https://enigmatic-inlet-99767.herokuapp.com/api/products/${_id}/delete`, {
                    method: "DELETE",
                    headers: {
                        "Content-Type": "application/json",
                        Authorization: `Bearer ${user.token}`
                    }
                })
                    .then(res => res.json())
                    .then(data => {
                        if (data == true) {
                            Swal.fire({
                                icon: 'success',
                                timer: 1000,
                                color: 'white',
                                background: 'rgb(125, 125, 125)',
                                backdrop: `url("${images[1]}") right bottom no-repeat`,
                                showClass: {
                                    popup: 'animate__animated animate__fadeInUp animate__faster'
                                },
                                hideClass: {
                                    popup: 'animate__animated animate__fadeOutUp animate__faster'
                                }
                            })
                            history.push("/view-my-illustrations")
                        }
                    })
            }
        })
    }


    let location = useLocation()
    const [path, setPath] = useState(location.pathname)

    useEffect(() => {
        getIllustrationDetails().then(() => {
            getSellerIllustrations()
        })

    }, [location.pathname, name, reload])

    return (
        <Fragment>
            <AppNavbar />
            {
                (loadingViewIllustration === true && firstTimeLoad === true) ?
                    <Container fluid className="illustration-box d-flex justify-content-center align-items-center">
                        <Container fluid className="p-0 d-flex justify-content-center align-items-center h-100">
                            <Spinner color={color} loading={loadingViewIllustration} />

                        </Container>

                    </Container>
                    :
                    <Fragment>
                        <Container fluid className="illustration-box d-flex justify-content-center">
                            <Container fluid className="p-0 d-flex justify-content-center">
                                <Row className='w-100 p-0 my-5'>
                                    <Col className="col-sm-12 col-lg-8">
                                        <Fade>
                                            <Card className="card-product d-flex justify-content-center align-items-center border" style={{ width: '100%', height: '85vh' }}>

                                                <Card.Img
                                                    onClick={() => handleShow()}
                                                    className="rounded illustration-view-hover product-preview-image"
                                                    variant="top"
                                                    src={productPicture}
                                                />

                                            </Card>
                                        </Fade>
                                    </Col>
                                    <Col className="col-sm-12 col-lg-4">
                                        <Row className="">
                                            <Col className='mb-5 p-0 m-0 col-12 d-flex justify-content-start'>
                                                <Button variant="secondary" onClick={() => history.goBack()}>Go Back</Button>
                                            </Col>
                                            <Col className='p-0 m-0 col-2 d-flex justify-content-center align-items-center'>
                                                <Link to={`/view-profile/${sellerId}`}>
                                                    <Avatar
                                                        round={true}
                                                        size="50"
                                                        name={nickname}
                                                        src={profilePicture}
                                                    />
                                                </Link>
                                            </Col>
                                            <Col className="col-10 d-flex justify-content-start align-items-center">
                                                <Link to={`/view-profile/${sellerId}`}>
                                                    <h4>{nickname}</h4>
                                                </Link>
                                            </Col>
                                            {
                                                (sellersId === user.id) ?
                                                    <Fragment>
                                                        <Form onSubmit={(e) => updateProduct(e)}>
                                                            <Col className='mt-5 col-12 d-flex justify-content-start align-items-center'>
                                                                <Row className="w-100 p-0 m-0">
                                                                    <Col className="d-flex justify-content-end align-items-center">
                                                                        <h4 className="mt-2">Title:</h4>
                                                                    </Col>
                                                                    <Col className="col-10">
                                                                        <Form.Group className="d-flex justify-content-center align-items-center">
                                                                            <Form.Control
                                                                                name="productName"
                                                                                type="text"
                                                                                placeholder="Name"
                                                                                id="title"
                                                                                defaultValue={name}
                                                                                onChange={(e) => setProductNameUpdate(e.target.value)}
                                                                                required
                                                                            />
                                                                            <Button variant="secondary" onClick={(e) => resetTitleProduct(e)}> X</Button>
                                                                        </Form.Group>
                                                                    </Col>
                                                                </Row>
                                                            </Col>
                                                            <Col className='mt-3 col-12 d-flex justify-content-start align-items-center'>
                                                                <Row className="w-100 p-0 m-0">
                                                                    <Col className="d-flex justify-content-end align-items-center">
                                                                        <h4 className="mt-2">Price:</h4>
                                                                    </Col>
                                                                    <Col className="col-10">
                                                                        <Form.Group className="d-flex justify-content-center align-items-center">
                                                                            <Form.Control
                                                                                name="productPrice"
                                                                                type="number"
                                                                                placeholder="Price"
                                                                                id="price"
                                                                                defaultValue={price}
                                                                                onChange={(e) => setProductPriceUpdate(e.target.value)}
                                                                                required
                                                                            />
                                                                            <Button variant="secondary" onClick={(e) => resetPriceProduct(e)}> X</Button>
                                                                        </Form.Group>
                                                                    </Col>
                                                                </Row>
                                                            </Col>
                                                            <Col className='mt-2 col-12 d-flex justify-content-start align-items-center'>
                                                                <Row className="w-100 p-0 m-0">
                                                                    <Col className="d-flex justify-content-center align-items-center">
                                                                        <h4 className="mt-2">Description</h4>
                                                                    </Col>
                                                                    <Col className="col-12">
                                                                        <Form.Group className="d-flex justify-content-center align-items-center">
                                                                            <Form.Control
                                                                                name="productDescription"
                                                                                type="text"
                                                                                as="textarea"
                                                                                placeholder="Description"
                                                                                id="description"
                                                                                defaultValue={description}
                                                                                onChange={(e) => setProductDescriptionUpdate(e.target.value)}
                                                                                required
                                                                            />
                                                                        </Form.Group>
                                                                    </Col>
                                                                    <Col className="col-12 d-flex justify-content-start align-items-center">
                                                                        <Button variant="secondary" className="w-100" onClick={(e) => resetDescriptionProduct(e)}> X</Button>
                                                                    </Col>
                                                                </Row>
                                                            </Col>
                                                            <Col className='my-3 col-12 d-flex justify-content-start align-items-center'>
                                                                <Row className="w-100 p-0 m-0">
                                                                    <Col className="col-12 d-flex justify-content-center align-items-center">
                                                                        <h4 className="mt-2">Change Illustration Image</h4>
                                                                    </Col>
                                                                    <Col className="col-12">
                                                                        <Form.Group className="d-flex justify-content-end align-items-center m-0 p-0">
                                                                            <Form.Control
                                                                                id="illustration-canvas-edit"
                                                                                onChange={onChangePicture}
                                                                                className="w-100 btn text-white btn-secondary"
                                                                                name="newImage"
                                                                                accept="image/png, image/jpeg"
                                                                                type="file"
                                                                            />
                                                                            <Button variant="danger" onClick={(e) => unsetImageProduct(e)}>X</Button>
                                                                        </Form.Group>
                                                                    </Col>
                                                                </Row>
                                                            </Col>
                                                            <Row>
                                                                <Col className='mt-2 col-12 d-flex justify-content-start align-items-center'>
                                                                    {
                                                                        (disableSubmit === true) ?
                                                                            <Button variant="primary btn-login border-bottom w-100 h-100 rounded-0" type="submit" disabled>
                                                                                Update Canvas Details
                                                                            </Button>
                                                                            :
                                                                            <Button variant="primary btn-login border-bottom w-100 h-100 rounded-0" type="submit">
                                                                                Update Canvas Details
                                                                            </Button>
                                                                    }

                                                                </Col>
                                                            </Row>
                                                        </Form>

                                                        {
                                                            (isActive !== true) ?
                                                                <Col className="col-12 mt-2 d-flex justify-content-center">
                                                                    <Button className="w-100" variant="primary" onClick={(e) => unarchiveProduct(e)}>Unarchive</Button>
                                                                </Col>
                                                                :
                                                                <Col className="col-12 mt-2 d-flex justify-content-center">
                                                                    <Button className="w-100" variant="secondary" onClick={(e) => archiveProduct(e)}>Archive</Button>
                                                                </Col>
                                                        }
                                                        <Col className="col-12 mt-2 d-flex justify-content-center">
                                                            <Button className="w-100" variant="danger" onClick={(e) => deleteProduct(e, productId, name)}>Delete Illustration</Button>
                                                        </Col>

                                                        {
                                                            (disableSubmit === true) ?
                                                                <Row className="p-0 m-0">
                                                                    <Col className="col-12 p-0 m-0">
                                                                        {progressInstance}
                                                                    </Col>
                                                                    <Col className="col-12 d-flex justify-content-center">
                                                                        <Spinner2 color={color} loading={loading} />
                                                                    </Col>
                                                                </Row>
                                                                :
                                                                <></>
                                                        }
                                                    </Fragment>
                                                    :
                                                    <Fragment>
                                                        <Col className='mt-4 col-12 d-flex justify-content-start align-items-center'>
                                                            {
                                                                (user.token !== null && user.isAdmin !== true) ?
                                                                    <Heart
                                                                        className="like-view-page-button"
                                                                        isActive={activeFavorites}
                                                                        onClick={() => setAsFavorite()}
                                                                        style={
                                                                            {
                                                                                fill: activeFavorites ? "red" : "white",
                                                                                stroke: activeFavorites ? "red" : "gray",
                                                                                filter: "drop-shadow(0px 3px 3px rgba(0, 0, 0, 1))"
                                                                            }
                                                                        }
                                                                    />
                                                                    :
                                                                    <></>
                                                            }
                                                        </Col>
                                                        <Col className='mt-3 col-12 d-flex justify-content-center align-items-center'>
                                                            <h4 className="mt-2">{`${name}`}</h4>
                                                        </Col>
                                                        <Col className='mt-3 col-12 d-flex justify-content-start align-items-center'>
                                                            <h4>Price: {price}</h4>
                                                        </Col>
                                                        <Col className='mt-2 col-12 d-flex justify-content-start align-items-center'>
                                                            <h4 className="mt-2">Description: {description}</h4>
                                                        </Col>
                                                        {
                                                            (user.isAdmin !== true) ?
                                                                <Form onSubmit={(e) => addToCart(e)}>
                                                                    <Row>
                                                                        <Col className='mt-2 col-5'>
                                                                            <Form.Group className="d-flex justify-content-start align-items-center">
                                                                                <Form.Control
                                                                                    name="quantity"
                                                                                    type="number"
                                                                                    placeholder="Qty"
                                                                                    value={quantity}
                                                                                    min={1}
                                                                                    onChange={(e) => setQuantity(e.target.value)}
                                                                                    required
                                                                                />
                                                                            </Form.Group>
                                                                        </Col>
                                                                        <Col className='mt-2 col-6 d-flex justify-content-start align-items-center'>
                                                                            <Button variant="primary btn-login border-bottom w-100 h-100 rounded-0" type="submit">
                                                                                Buy this Illustration Canvas
                                                                            </Button>
                                                                        </Col>
                                                                    </Row>
                                                                </Form>
                                                                :
                                                                <Row>
                                                                    <Col className='mt-2 col-12 d-flex justify-content-start align-items-center'>
                                                                        {
                                                                            isActive === true ?
                                                                                <Button variant="danger" className="w-100" onClick={() => setIsActiveFeatured(productId, "archive")}>Archive</Button>
                                                                                :
                                                                                <Button variant="success" className="w-100" onClick={() => setIsActiveFeatured(productId, "unarchive")}>Unarchive</Button>
                                                                        }
                                                                    </Col>
                                                                    {
                                                                        isActive === true ?
                                                                            <Col className='mt-2 col-12 d-flex justify-content-start align-items-center'>
                                                                                {
                                                                                    isFeatured === true ?
                                                                                        <Button variant="danger" className="w-100" onClick={() => setIsActiveFeatured(productId, "unfeature")}>Remove Featured</Button>
                                                                                        :
                                                                                        <Button variant="success" className="w-100" onClick={() => setIsActiveFeatured(productId, "feature")}>Set Featured</Button>
                                                                                }
                                                                            </Col>
                                                                            :
                                                                            <></>
                                                                    }
                                                                </Row>

                                                        }

                                                    </Fragment>
                                            }
                                        </Row>
                                    </Col>
                                </Row>

                            </Container>



                        </Container>
                        <Container fluid className="illustration-box d-flex justify-content-center">
                            {
                                (sellerIllustrations.length !== 0) ?
                                    <Container fluid className="p-0 m-0 d-flex justify-content-center">
                                        <Row className="p-0 m-0 w-100">
                                            <Col className="universal-box col-lg-12 col-md-12 col-sm-12 p-0 mt-2">
                                                <Row className="w-100 p-0 m-0 d-flex justify-content-center">
                                                    {
                                                        (sellersId !== user.id) ?
                                                            <Col className="mt-5 col-12 p-0 m-0  d-flex justify-content-center">
                                                                <Spinner color={color} loading={loadingIllustration} />
                                                                <h1>Other Illustration Canvas by this seller</h1>
                                                                <Spinner color={color} loading={loadingIllustration} />
                                                            </Col>
                                                            :
                                                            <Col className="mt-5 col-12 p-0 m-0  d-flex justify-content-center">
                                                                <Spinner color={color} loading={loadingIllustration} />
                                                                <h1>Other Illustration Canvas by you</h1>
                                                                <Spinner color={color} loading={loadingIllustration} />
                                                            </Col>
                                                    }
                                                    {sellerIllustrations}
                                                    <Col className="col-12">
                                                        <ReactPaginate
                                                            previousLabel={'<'}
                                                            nextLabel={'>'}
                                                            breakLabel={'...'}
                                                            pageCount={pageCount}
                                                            marginePagesDisplayed={3}
                                                            pageRangeDisplayed={3}
                                                            onPageChange={handlePageClick}
                                                            forcePage={(localStorage.getItem("sellerIllustrationsPage") - 1)}
                                                            containerClassName={'pagination justify-content-center'}
                                                            pageClassName={'page-item'}
                                                            pageLinkClassName={'page-link'}
                                                            previousClassName={'page-item'}
                                                            previousLinkClassName={'page-link'}
                                                            nextClassName={'page-item'}
                                                            nextLinkClassName={'page-link'}
                                                            breakClassName={'page-item'}
                                                            breakLinkClassName={'page-link'}
                                                            activeClassName={'active'}
                                                        />
                                                    </Col>
                                                </Row>
                                            </Col>
                                        </Row>
                                    </Container>
                                    :
                                    <></>
                            }
                        </Container>
                    </Fragment>
            }
            <Modal
                show={showIllustrationModal}
                fullscreen={true}
                onHide={() => setShowIllustrationModal(false)}
                className="illustration-modal"
            >
                <Modal.Header className="illustration-modal">
                    <Modal.Title className="text-center w-100">{name}</Modal.Title>

                    <CloseButton variant="white" onClick={() => setShowIllustrationModal(false)} />
                </Modal.Header>
                <Modal.Body className="illustration-modal d-flex justify-content-center">
                    <Container fluid className="d-flex justify-content-center">
                        <Scrollbars
                            renderThumbHorizontal={({ style, ...props }) =>
                                <div {...props} style={{ ...style, width: '3px', borderRadius: '4px', boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.16)', backgroundColor: '#be913d' }} />
                            }
                            renderTrackVertical={({ style, ...props }) =>
                                <div {...props} style={{ ...style, backgroundColor: 'black', right: '2px', bottom: '2px', top: '2px', borderRadius: '3px', width: '5px' }} />
                            }
                            renderThumbVertical={({ style, ...props }) =>
                                <div {...props} style={{ ...style, width: '3px', borderRadius: '4px', boxShadow: '0 2px 4px 0 rgba(0, 0, 0, 0.16)', backgroundColor: '#be913d' }} />
                            }>
                            <Container fluid className="d-flex justify-content-center">
                                <Image className="product-picture-modal" src={productPicture} />
                            </Container>

                        </Scrollbars>
                    </Container>
                </Modal.Body>
            </Modal>
        </Fragment>
    )
}
