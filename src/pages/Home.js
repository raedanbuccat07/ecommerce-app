import { Fragment, useContext } from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import AppNavbar from '../components/AppNavbar'
import FeaturedArtContainer from '../components/FeaturedArtContainer'
import { NavLink, Link } from 'react-router-dom'
import UserContext from '../UserContext'
import { Fade } from "react-awesome-reveal"

export default function Home() {
	const { user, setUser } = useContext(UserContext);
	return (
		<Fragment>
			<AppNavbar />
			<Fade>
				<Container fluid className="d-flex align-items-center p-0 m-0 home-outside-container">
					<Container className="home-container d-flex justify-content-center align-items-center">
						<Row className="w-100 p-0 m-0 welcome-box">
							<Col className="col-12 p-0 m-0 d-flex justify-content-center align-items-center">
								{
									(user.id !== null) ?
										<h1 className="text-white mt-3">Welcome to EMINA, {user.nickname}</h1>
										:
										<h1 className="text-white mt-3">Welcome to EMINA</h1>
								}
							</Col>
							<Col className="col-12 p-0 m-0 d-flex justify-content-center align-items-center">
								<h1 className="mb-4 text-white text-center">Anime Illustration Canvas shop</h1>
							</Col>
							<Col className="col-12 p-0 m-0 d-flex justify-content-center align-items-center">
								<h4 className="mb-4 mb-md-5 text-white text-center">Emina Mise is a online ecommerce for artists and art enthusiasts, and a platform for emerging and established artists to exhibit, promote, and sell their works with an enthusiastic, art-centric community.</h4>
							</Col>
							<Col className="col-12 p-0 m-0 d-flex justify-content-center align-items-center">
								{
									(user.isAdmin !== true) ?
										<Link className="btn home-shop-button text-white p-3 px-xl-4 py-xl-3 mb-3" as={NavLink} to="/illustrations">Shop Now</Link>
										:
										<Link className="btn home-shop-button text-white p-3 px-xl-4 py-xl-3 mb-3" as={NavLink} to="/illustrations">View Illustrations</Link>
								}
							</Col>
						</Row>
					</Container>
				</Container>
			</Fade>
			<Fade direction='in'>
				<FeaturedArtContainer />
			</Fade>
		</Fragment>
	)
}
