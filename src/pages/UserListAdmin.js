import React, { Fragment, useState, useContext, useEffect } from 'react';
import { Container, Row, Col, Button } from 'react-bootstrap';
import { Redirect, useHistory } from 'react-router-dom'
import AppNavbar from '../components/AppNavbar'
import UserContext from '../UserContext'
import axios from 'axios'
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter'
import paginationFactory from 'react-bootstrap-table2-paginator';
import Avatar from 'react-avatar';
import Spinner from "react-spinners/ScaleLoader";

export default function UserListAdmin() {

	let [loading, setLoading] = useState(true);
	let [loading2, setLoading2] = useState(false);
	let [color, setColor] = useState("#ffffff");
	//<Spinner color={color} loading={true} />

	const history = useHistory()
	const { user, setUser } = useContext(UserContext);
	//
	const images = importAll(require.context('../images/swalBackdrop', false, /\.(gif|png|jpe?g|svg)$/));
	function importAll(r) {
		return r.keys().map(r);
	}

	const [data, setData] = useState([])

	const getData = () => {
		setLoading2(true)
		const config = {
			headers: { Authorization: `Bearer ${user.token}` }
		}
		axios("https://enigmatic-inlet-99767.herokuapp.com/api/users/all").then((res) => {
			setData(res.data)
		})
		setLoading(false)
		setLoading2(false)
	}


	const nicknameFormatter = (data, row) => {
		return <span>
			<Row>
				<Col className="d-flex justify-content-center col-auto">
					<Avatar
						round={true}
						size="50"
						name={row.nickname}
						src={row.profilePicture}
					/>
				</Col>
				<Col className="col-auto d-flex justify-content-start align-items-center">
					<h4>{row.nickname}</h4>
				</Col>
			</Row>
		</span>
	}

	const headerFormat = (column, colIndex, { sortElement, filterElement }) => {
		return <Row>
			<Col className="col-12 my-2">
				<h5>{column.text} {sortElement}</h5>
			</Col>
			<Col className="col-12">
				{filterElement}
			</Col>
		</Row>
	}

	const viewProfileFormat = (data, row) => {
		return <span>
			<Row>
				<Col className="d-flex justify-content-center col-12 mt-2">
					{
						row.email !== "admin@gmail.com" ?
							<Button variant="danger" className="w-100" onClick={() => goToProfile(row)}>Go to profile</Button>
							:
							<></>
					}
				</Col>
			</Row>
		</span>
	}

	const goToProfile = (data) => {
		history.push(`/view-profile/${data._id}`)
	}

	const columns = [
		{
			dataField: 'nickname',
			text: 'Nickname',
			formatter: nicknameFormatter,
			headerFormatter: (headerFormat),
			headerStyle: (colum, colIndex) => {
				return {
					textAlign: 'center',
					color: 'white',
					backgroundColor: '#634a1b',
					cursor: 'pointer',
				};
			},
			style: {
				color: 'white',
				cursor: 'pointer'
			},
			filter: textFilter(),
			sort: true,
		},
		{
			dataField: '_id',
			text: 'ID',
			headerFormatter: (headerFormat),
			headerStyle: (colum, colIndex) => {
				return {
					textAlign: 'center',
					color: 'white',
					backgroundColor: '#634a1b',
					cursor: 'pointer',
				};
			},
			style: {
				color: 'white',
				cursor: 'pointer'
			},
			filter: textFilter(),
			sort: true,
		},
		{
			dataField: 'email',
			text: 'Email',
			headerFormatter: (headerFormat),
			headerStyle: (colum, colIndex) => {
				return {
					textAlign: 'center',
					color: 'white',
					backgroundColor: '#634a1b',
					cursor: 'pointer',
				};
			},
			style: {
				color: 'white',
				cursor: 'pointer'
			},
			filter: textFilter(),
			sort: true,
		},
		{
			dataField: 'gender',
			text: 'Gender',
			headerFormatter: (headerFormat),
			headerStyle: (colum, colIndex) => {
				return {
					textAlign: 'center',
					color: 'white',
					backgroundColor: '#634a1b',
					cursor: 'pointer',
				};
			},
			style: {
				color: 'white',
				cursor: 'pointer'
			},
			filter: textFilter(),
			sort: true,
		},
		{
			dataField: 'Action',
			text: '',
			formatter: viewProfileFormat,
			headerFormatter: (headerFormat),
			headerStyle: (colum, colIndex) => {
				return {
					textAlign: 'center',
					color: 'white',
					backgroundColor: '#634a1b',
					cursor: 'pointer',
				};
			},
			style: {
				color: 'white',
				cursor: 'pointer'
			},
		},
	]

	const defaultSorted = [{
		dataField: 'nickname',
		order: 'asc'
	}]

	const options = {
		paginationSize: 4,
		pageStartIndex: 0,
		firstPageText: 'First',
		prePageText: 'Back',
		nextPageText: 'Next',
		lastPageText: 'Last',
		nextPageTitle: 'First page',
		prePageTitle: 'Pre page',
		firstPageTitle: 'Next page',
		lastPageTitle: 'Last page',
		showTotal: true,
		disablePageTitle: true,
		sizePerPageList: [{
			text: '5', value: 5
		}, {
			text: '10', value: 10
		}, {
			text: '25', value: 25
		}, {
			text: '50', value: 50
		}, {
			text: 'All', value: data.length
		}]
	};


	useEffect(() => {
		getData()
	}, [])

	// || user.isAdmin !== true
	return (
		(user.token == null) ?
			<Redirect to="/login" />
			:
			<Fragment>
				<AppNavbar />
				{
					(loading === true) ?
						<Container fluid className="illustration-box d-flex justify-content-center align-items-center">
							<Container fluid className="p-0 m-0 d-flex justify-content-center align-items-center">
								<Row className="p-0 m-0 w-100 d-flex justify-content-center align-items-center">
									<Col className="universal-box col-lg-12 col-md-12 col-sm-12 p-0 mt-2 d-flex justify-content-center align-items-center">
										<Spinner color={color} loading={loading} />
									</Col>
								</Row>
							</Container>
						</Container>
						:
						<Fragment>
							<Container fluid className="illustration-box">
								<Container fluid className="p-0 m-0 d-flex justify-content-center">
									<Row className="p-0 m-0 w-100">
										<Col className="universal-box col-lg-12 col-md-12 col-sm-12 p-0 mt-2">
											<Row className="w-100 p-0 m-0 d-flex justify-content-center">
												<Fragment>
													<Col className="mt-3 col-12 p-0 m-0 d-flex justify-content-center">
													<Spinner color={color} loading={loading2} /><h1>User List</h1><Spinner color={color} loading={loading2} />
													</Col>

													<Col className="mt-3 col-12 p-0 m-0">
														<BootstrapTable
															striped
															hover
															condensed
															bootstrap4
															keyField="_id"
															data={data}
															columns={columns}
															defaultSorted={defaultSorted}
															filter={filterFactory()}
															pagination={paginationFactory(options)}
														/>
													</Col>
												</Fragment>
											</Row>
										</Col>
									</Row>
								</Container>
							</Container>
						</Fragment>
				}

			</Fragment>
	)
}
